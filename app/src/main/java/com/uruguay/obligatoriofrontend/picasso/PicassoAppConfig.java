package com.uruguay.obligatoriofrontend.picasso;

import android.content.Context;
import android.net.Uri;
import android.util.Log;

import com.squareup.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;
import com.uruguay.obligatoriofrontend.retrofit.SelfSigningClientBuilder;

import okhttp3.OkHttpClient;

import static com.uruguay.obligatoriofrontend.StaticStrings.LOG_TAG;

public class PicassoAppConfig {

    public static Picasso getPicasso(Context context) {

        OkHttpClient okHttpClient = SelfSigningClientBuilder.createClient(context);
        OkHttp3Downloader okHttpDownloader = new OkHttp3Downloader(okHttpClient);

        return new Picasso.Builder(context)
                .downloader(okHttpDownloader)
                .listener(new Picasso.Listener() {
                    @Override
                    public void onImageLoadFailed(Picasso picasso, Uri uri, Exception exception) {
                        Log.d(LOG_TAG,"PicassoAppConfig: error: " + exception.getMessage());
                        exception.printStackTrace();
                    }
                })
                .build();
    }
}