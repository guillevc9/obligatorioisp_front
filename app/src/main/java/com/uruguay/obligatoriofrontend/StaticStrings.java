package com.uruguay.obligatoriofrontend;

public final class StaticStrings {

    public static final String LOG_TAG = "Obligatorio";
    public static final String AUTHORIZATION_FLAG = "Authorization";
    public static final String FLAG_UPDATE_PUBLICATION = "Update_Publication";
    public static final String FLAG_LOCAL = "photo_local";
    public static final String FLAG_EXIT = "EXIT";
    public static final String FLAG_MY_PUBLICATIONS = "mypublications";
    public static final String FLAG_MY_PUBLICATIONS_ANNOTATED = "mypublicationsannotated";
    public static final String FLAG_MYPUBLICATIONS_OR_ANNOTATED = "mypublicationsannotated";
    public static final String UNAUTHORIZED = "Usuario no autorizado";
    public static final String ID = "id";
    public static final String TITLE = "title";
    public static final String CATEGORY = "category";
    public static final String DESCRIPTION = "description";
    public static final String ADDRESS = "address";
    public static final String ZONE = "zone";
    public static final String PUBLISHED_BY_ID = "publishedBy";
    public static final String PUBLISHED_BY_NAME = "publishedByName";
    public static final String PUBLISHED_BY_FAMILY_NAME = "publishedByFamilyName";
    public static final String STATE = "state";
    public static final String STATE_ACTIVE = "Activo";
    public static final String STATE_FINALIZE = "Finalizado";
    public static final String REASON = "REASON";
    public static final String USER_ASSIGNED = "USER_ASSIGNED";
    public static final String USER_ASSIGNED_ID = "USER_ASSIGNED_ID";
    public static final String OTHER_REASON = "OTHER_REASON";
    public static final String REASON_SELECT_CANDIDATE = "Seleccionar candidato";
    public static final String REASON_NO_USERS = "No se anotaron candidatos";
    public static final String REASON_OTHER = "Otro motivo";
    public static final String PHOTOS = "photos";
    public static final String IMAGE = "IMAGE";
    public static final String QUESTION = "QUESTION";
    public static final String QUESTION_ID = "QUESTIO_ID";
    public static final String ANSWER = "ANSWER";
    public static final String PUBLICATION_ID = "PUBLICATION_ID";
    public static final String TITLE_VIEW_NEW_PUBLICATION = "Crear Publicación";
    public static final String ADD_PUBLICATION_OK = "Publicación agregada correctamente";
    public static final String ADD_PUBLICATION_ERROR = "Error al agregar la publicación";
    public static final String TITLE_VIEW_HOME = "Publicaciones";
    public static final String READ_PUBLICATIONS_FROM_SERVER_ERROR = "No se puede conectar con el servidor.";
    public static final String UPDATE_PUBLICATIONS_OK = "Publicación actualizada correctamente.";
    public static final String UPDATE_PUBLICATIONS_ERROR = "Error al actualizar la Publicación";
    public static final String UPLOAD_PHOTOS_ERROR = "Error al subir las fotos.\"";
    public static final String TITLE_MY_PUBLICATIONS_VIEW = "Mis Publicaciones";
    public static final String TITLE_MY_PUBLICATIONS_ANNOTATED_VIEW = "Anotadas";
    public static final String TITLE_FINALIZE_PUBLICATION_VIEW = "Finalizar Publicación";
    public static final String TITLE_PUBLICATION_DETAILS_VIEW = "Publicación";
    public static final String TITLE_LOGIN_VIEW = "Login";
    public static final String TITLE_MAP_VIEW = "Como llegar";
    public static final String TITLE_NEW_QUESTION_VIEW = "Nueva Pregunta";
    public static final String NEW_QUESTION_OK = "Pregunta enviada correctamente.";
    public static final String NEW_QUESTION_ERROR = "Error al enviar la pregunta.";
    public static final String NEW_QUESTION_EMPTY = "No puedes enviar una pregunta vacía.";
    public static final String TITLE_NEW_ANSWER_VIEW = "Responder";
    public static final String NEW_ANSWER_OK = "Respuesta enviada correctamente.";
    public static final String NEW_ANSWER_ERROR = "Error al enviar la respuesta.";
    public static final String NEW_ANSWER_EMPTY = "No puedes enviar una respuesta vacía.";
    public static final String OPERATION_OK = "Operación realizada correctamente.";
    public static final String CONECTION_ERROR = "Error al conectar con el servidor.";
    public static final String TITLE_MODIFY_PUBLICATION_VIEW = "Modificar Publicación";
    public static final String BTN_MODIFY_PUBLICATION = "Modificar";
    public static final String GPS_OFF = "Debes prender el GPS para utilizar esta funcionalidad";
    public static final String GPS_NOT_LOCATION = "No se pudo obtener ubicación";
    public static final String GET_QUESTIONS_ERROR = "Error al cargar las preguntas.";
    public static final String FINALIZE_PUBLICATION_OK =  "Publicación finalizada correctamente.";
    public static final String GET_CANDIDATES_ERROR = "No se pudieron cargar los candidatos anotados.";
    public static final String MOTIVE_MANDATORY = "Debe ingresar el motivo.";
    public static final String CONECTION_FAILED = "No se pudo conectar con el servidor.";




}
