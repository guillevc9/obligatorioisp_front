package com.uruguay.obligatoriofrontend;

public final class HttpCodes {

    public static final int OK = 200;
    public static final int CREATED = 201;
    public static final int UNAUTHORIZED = 401;

}