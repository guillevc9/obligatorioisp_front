package com.uruguay.obligatoriofrontend.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.uruguay.obligatoriofrontend.R;
import com.uruguay.obligatoriofrontend.dto.QuestionDTO;
import com.uruguay.obligatoriofrontend.ui.newanswer.NewAnswerView;

import java.util.ArrayList;
import java.util.List;

import static com.uruguay.obligatoriofrontend.StaticStrings.ANSWER;
import static com.uruguay.obligatoriofrontend.StaticStrings.LOG_TAG;
import static com.uruguay.obligatoriofrontend.StaticStrings.PUBLICATION_ID;
import static com.uruguay.obligatoriofrontend.StaticStrings.QUESTION;
import static com.uruguay.obligatoriofrontend.StaticStrings.QUESTION_ID;

public class QuestionAdapter extends RecyclerView.Adapter<QuestionAdapter.QuestionViewHolder> {

    private List<QuestionDTO> questions;
    private int idPublication;
    private boolean owner;
    private boolean active;

    public QuestionAdapter(List<QuestionDTO> questions, int idPublication, boolean owner, boolean active) {
        this.questions = questions;
        this.idPublication = idPublication;
        this.owner = owner;
        this.active = active;
    }

    @NonNull
    @Override
    public QuestionViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_question, viewGroup, false);
        return new QuestionViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull QuestionViewHolder questionViewHolder, int i) {
        QuestionDTO question = questions.get(i);
        questionViewHolder.tvQuestion.setText(question.getQuestion());
        String answer = question.getAnswer();
        if (answer.isEmpty()) {
            questionViewHolder.imgAnswer.setVisibility(ImageView.GONE);
            questionViewHolder.tvAnswer.setVisibility(TextView.GONE);
        } else {
            questionViewHolder.imgAnswer.setVisibility(ImageView.VISIBLE);
            questionViewHolder.tvAnswer.setVisibility(TextView.VISIBLE);
            questionViewHolder.tvAnswer.setText(question.getAnswer());
        }
    }

    @Override
    public int getItemCount() {
        return questions.size();
    }

    public class QuestionViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView tvQuestion;
        private ImageView imgAnswer;
        private TextView tvAnswer;

        public QuestionViewHolder(@NonNull View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            tvQuestion = itemView.findViewById(R.id.tvRecyQuestion);
            imgAnswer = itemView.findViewById(R.id.imgRecyAnswer);
            tvAnswer = itemView.findViewById(R.id.tvRecyAnswer);
        }

        @Override
        public void onClick(View v) {
            if (owner && active) {
                Context context = v.getContext();
                int position = getAdapterPosition();
                QuestionDTO question = questions.get(position);
                Log.d(LOG_TAG, "QuestionAdapter: " + questions.toString() + "");
                Intent intent = new Intent(context, NewAnswerView.class);
                intent.putExtra(PUBLICATION_ID, idPublication);
                intent.putExtra(QUESTION_ID, question.getQuestionId());
                intent.putExtra(QUESTION, question.getQuestion());
                intent.putExtra(ANSWER, question.getAnswer());
                context.startActivity(intent);
            }
        }

    }

}
