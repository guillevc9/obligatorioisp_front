package com.uruguay.obligatoriofrontend.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.uruguay.obligatoriofrontend.R;
import com.uruguay.obligatoriofrontend.dto.PublicationDTO;
import com.uruguay.obligatoriofrontend.ui.showpublicatondetails.ShowPublicationDetailsView;

import java.util.List;

import static com.uruguay.obligatoriofrontend.StaticStrings.ADDRESS;
import static com.uruguay.obligatoriofrontend.StaticStrings.CATEGORY;
import static com.uruguay.obligatoriofrontend.StaticStrings.DESCRIPTION;
import static com.uruguay.obligatoriofrontend.StaticStrings.ID;
import static com.uruguay.obligatoriofrontend.StaticStrings.LOG_TAG;
import static com.uruguay.obligatoriofrontend.StaticStrings.OTHER_REASON;
import static com.uruguay.obligatoriofrontend.StaticStrings.PHOTOS;
import static com.uruguay.obligatoriofrontend.StaticStrings.PUBLISHED_BY_FAMILY_NAME;
import static com.uruguay.obligatoriofrontend.StaticStrings.PUBLISHED_BY_ID;
import static com.uruguay.obligatoriofrontend.StaticStrings.PUBLISHED_BY_NAME;
import static com.uruguay.obligatoriofrontend.StaticStrings.REASON;
import static com.uruguay.obligatoriofrontend.StaticStrings.STATE;
import static com.uruguay.obligatoriofrontend.StaticStrings.STATE_ACTIVE;
import static com.uruguay.obligatoriofrontend.StaticStrings.STATE_FINALIZE;
import static com.uruguay.obligatoriofrontend.StaticStrings.TITLE;
import static com.uruguay.obligatoriofrontend.StaticStrings.USER_ASSIGNED;
import static com.uruguay.obligatoriofrontend.StaticStrings.USER_ASSIGNED_ID;
import static com.uruguay.obligatoriofrontend.StaticStrings.ZONE;

public class PublicationAdapter extends RecyclerView.Adapter<PublicationAdapter.PublicationViewHolder> {

    private static final int MAX_CHARACTERS_DESCRIPTION = 50;
    private static final int START_INDEX_DESCRIPTION = 0;
    private List<PublicationDTO> publications;

    public PublicationAdapter(List<PublicationDTO> publications) {
        this.publications = publications;
    }

    @NonNull
    @Override
    public PublicationViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_publication, viewGroup, false);
        return new PublicationViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PublicationViewHolder publicationViewHolder, int i) {
        PublicationDTO publication = publications.get(i);
        publicationViewHolder.tvTitle.setText(publication.getTitle());
        publicationViewHolder.tvCategory.setText(publication.getCategory());
        publicationViewHolder.tvZone.setText(publication.getZone());
        String description = publication.getDescription();
        String descriptionToShow;
        if (description.length() > MAX_CHARACTERS_DESCRIPTION) {
            descriptionToShow = description.substring(START_INDEX_DESCRIPTION, MAX_CHARACTERS_DESCRIPTION);
            descriptionToShow += "...";
        } else {
            descriptionToShow = description;
        }
        publicationViewHolder.tvDescription.setText(descriptionToShow);
        String result;
        if (publication.isActive()) {
            result = STATE_ACTIVE;
        } else {
            result = STATE_FINALIZE;
        }
        publicationViewHolder.tvState.setText(result);
    }

    @Override
    public int getItemCount() {
        return publications.size();
    }

    public class PublicationViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView tvTitle;
        private TextView tvZone;
        private TextView tvDescription;
        private TextView tvCategory;
        private TextView tvState;

        public PublicationViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            tvCategory = itemView.findViewById(R.id.tvRecyCategoria);
            tvTitle = itemView.findViewById(R.id.tvRecyTitulo);
            tvZone = itemView.findViewById(R.id.tvRecyZone);
            tvDescription = itemView.findViewById(R.id.tvRecyDescription);
            tvState = itemView.findViewById(R.id.tvRecyState);
        }

        @Override
        public void onClick(View v) {
            Context context = v.getContext();
            int position = getAdapterPosition();
            PublicationDTO publication = publications.get(position);
            Log.d(LOG_TAG, "PublicationItemAdapter: " + publication.toString() + "");
            Intent intent = new Intent(context, ShowPublicationDetailsView.class);
            intent.putExtra(ID, publication.getId());
            intent.putExtra(TITLE, publication.getTitle());
            intent.putExtra(DESCRIPTION, publication.getDescription());
            intent.putExtra(ZONE, publication.getZone());
            intent.putExtra(ADDRESS, publication.getAddress());
            intent.putExtra(CATEGORY, publication.getCategory());
            intent.putExtra(PUBLISHED_BY_ID, publication.getUserId());
            intent.putExtra(PUBLISHED_BY_NAME, publication.getUserName());
            intent.putExtra(PUBLISHED_BY_FAMILY_NAME, publication.getUserFamilyName());
            intent.putExtra(STATE, publication.isActive());
            intent.putExtra(REASON, publication.getReason());
            intent.putExtra(USER_ASSIGNED, publication.getUserAssigned());
            intent.putExtra(USER_ASSIGNED_ID, publication.getUserAssignedId());
            intent.putExtra(OTHER_REASON, publication.getOtherReason());
            intent.putExtra(PHOTOS, publication.getPhotos());
            Log.d(LOG_TAG, "PublicationItemAdapter: PHOTOS" + publication.getPhotos() + "");
            context.startActivity(intent);
        }

    }

}
