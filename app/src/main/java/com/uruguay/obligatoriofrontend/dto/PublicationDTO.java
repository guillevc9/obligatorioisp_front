package com.uruguay.obligatoriofrontend.dto;

public class PublicationDTO {

    private boolean active;
    private int id;
    private String title;
    private String description;
    private String address;
    private String category;
    private String zone;
    private String userId;
    private String userName;
    private String userFamilyName;
    private String reason;
    private String userAssigned;
    private String userAssignedId;
    private String otherReason;
    private int photos;

    public PublicationDTO() {
        this.active = true;
        this.id = 0;
        this.title = "";
        this.description = "";
        this.address = "";
        this.category = "";
        this.zone = "";
        this.userId = "";
        this.userName = "";
        this.userFamilyName = "";
        this.reason = "";
        this.userAssigned = "";
        userAssignedId = "";
        this.otherReason = "";
        this.photos = 0;
    }

    public PublicationDTO(int id, String title, String description, String address, String category, String zone,
                          String userId, String userName, String userFamilyName, int photos) {
        this.active = true;
        this.id = id;
        this.title = title;
        this.description = description;
        this.address = address;
        this.category = category;
        this.zone = zone;
        this.userId = userId;
        this.userName = userName;
        this.userFamilyName = userFamilyName;
        this.reason = "";
        this.userAssigned = "";
        this.userAssignedId = "";
        this.otherReason = "";
        this.photos = photos;
    }

    public String getUserAssignedId() {
        return userAssignedId;
    }

    public void setUserAssignedId(String userAssignedId) {
        this.userAssignedId = userAssignedId;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getUserAssigned() {
        return userAssigned;
    }

    public void setUserAssigned(String userAssigned) {
        this.userAssigned = userAssigned;
    }

    public String getOtherReason() {
        return otherReason;
    }

    public void setOtherReason(String otherReason) {
        this.otherReason = otherReason;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getZone() {
        return zone;
    }

    public void setZone(String zone) {
        this.zone = zone;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserFamilyName() {
        return userFamilyName;
    }

    public void setUserFamilyName(String userFamilyName) {
        this.userFamilyName = userFamilyName;
    }

    public int getPhotos() {
        return photos;
    }

    public void setPhotos(int photos) {
        this.photos = photos;
    }

    @Override
    public String toString() {
        return "PublicationDTO{" +
                "active=" + active +
                ", id=" + id +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", address='" + address + '\'' +
                ", category='" + category + '\'' +
                ", zone='" + zone + '\'' +
                ", userId='" + userId + '\'' +
                ", userName='" + userName + '\'' +
                ", userFamilyName='" + userFamilyName + '\'' +
                ", reason='" + reason + '\'' +
                ", userAssigned='" + userAssigned + '\'' +
                ", userAssignedId='" + userAssignedId + '\'' +
                ", otherReason='" + otherReason + '\'' +
                ", photos=" + photos +
                '}';
    }
}
