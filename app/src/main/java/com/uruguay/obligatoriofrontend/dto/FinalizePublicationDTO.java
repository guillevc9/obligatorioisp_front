package com.uruguay.obligatoriofrontend.dto;

public class FinalizePublicationDTO {

    private int idPublication;
    private String reason;
    private String idGoogleUser;
    private String otherReason;

    public FinalizePublicationDTO() {
        this.idPublication = 0;
        this.reason = "";
        this.idGoogleUser = "";
        this.otherReason = "";
    }

    public FinalizePublicationDTO(int idPublication, String reason, String idGoogleUser, String otherReason) {
        this.idPublication = idPublication;
        this.reason = reason;
        this.idGoogleUser = idGoogleUser;
        this.otherReason = otherReason;
    }

    public int getIdPublication() {
        return idPublication;
    }

    public void setIdPublication(int idPublication) {
        this.idPublication = idPublication;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getIdGoogleUser() {
        return idGoogleUser;
    }

    public void setIdGoogleUser(String idGoogleUser) {
        this.idGoogleUser = idGoogleUser;
    }

    public String getOtherReason() {
        return otherReason;
    }

    public void setOtherReason(String otherReason) {
        this.otherReason = otherReason;
    }

    @Override
    public String toString() {
        return "FinalizePublicationDTO{" +
                "idPublication=" + idPublication +
                ", reason='" + reason + '\'' +
                ", idGoogleUser='" + idGoogleUser + '\'' +
                ", otherReason='" + otherReason + '\'' +
                '}';
    }
}
