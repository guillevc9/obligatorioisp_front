package com.uruguay.obligatoriofrontend.dto;

public class QuestionDTO {

    private int questionId;
    private String question;
    private String answer;

    public QuestionDTO() {
        this.questionId = 0;
        this.question = "";
        this.answer = "";
    }

    public QuestionDTO(int questionId, String question, String answer) {
        this.questionId = questionId;
        this.question = question;
        this.answer = answer;
    }

    public int getQuestionId() {
        return questionId;
    }

    public void setQuestionId(int questionId) {
        this.questionId = questionId;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    @Override
    public String toString() {
        return "question: " + question + "\n" +
                "answer: " + answer + "\n";
    }

}
