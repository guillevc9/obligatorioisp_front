package com.uruguay.obligatoriofrontend.dto;

public class UserDTO {

    private String name;
    private String familyName;
    private String idGoogle;

    public UserDTO() {
        this.name = "";
        this.familyName = "";
        this.idGoogle = "";
    }

    public UserDTO(String name, String familyName, String idGoogle) {
        this.name = name;
        this.familyName = familyName;
        this.idGoogle = idGoogle;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public String getIdGoogle() {
        return idGoogle;
    }

    public void setIdGoogle(String idGoogle) {
        this.idGoogle = idGoogle;
    }

    @Override
    public String toString() {
        return name + " " + familyName;
    }
}
