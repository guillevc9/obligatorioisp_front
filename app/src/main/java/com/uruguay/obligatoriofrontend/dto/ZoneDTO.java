package com.uruguay.obligatoriofrontend.dto;

public class ZoneDTO {
    private String name;

    public ZoneDTO(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
