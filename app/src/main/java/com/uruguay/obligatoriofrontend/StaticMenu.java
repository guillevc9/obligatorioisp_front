package com.uruguay.obligatoriofrontend;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;

import com.uruguay.obligatoriofrontend.ui.login.LoginView;

public final class StaticMenu {

    private static final String TITLE = "¿Salir de la aplicación?";
    private static final String BTN_EXIT = "Salir";
    private static final String BTN_CANCEL = "Cancel";

    public static void exit(final Context context) {
        AlertDialog.Builder alerta = new AlertDialog.Builder(context);
        alerta.setMessage(TITLE);
        alerta.setPositiveButton(BTN_EXIT, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Intent intent = new Intent(context.getApplicationContext(), LoginView.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra(StaticStrings.FLAG_EXIT, true);
                context.startActivity(intent);

            }
        });
        alerta.setNegativeButton(BTN_CANCEL, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
            }
        });
        alerta.show();
    }

}
