package com.uruguay.obligatoriofrontend.retrofit;

import com.google.gson.JsonPrimitive;
import com.uruguay.obligatoriofrontend.dto.*;
import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface IRetrofitApiConfigMVP {

    @GET("publication")
    Call<List<PublicationDTO>> getPublications(@Query("category")String category,@Query("zone")String zone);

    @GET("publication/created")
    Call<List<PublicationDTO>> getMyPublications();

    @GET("publication/annotated")
    Call<List<PublicationDTO>> getMyPublicationsAnnotated();

    @POST("publication")
    Call<PublicationDTO> newPublication(@Body PublicationDTO publication);

    @POST("publication/finalize")
    Call<Void> finalizePublication(@Body FinalizePublicationDTO finalizePublicationDTO);

    @PUT("publication")
    Call<PublicationDTO> updatePublication(@Body PublicationDTO publication);

    @GET
    Call<JsonPrimitive> getAnnotated(@Url String url);

    @GET("category")
    Call<List<CategoryDTO>> getCategories();

    @GET("zone")
    Call<List<ZoneDTO>> getZones();

    @GET
    Call<List<UserDTO>> getUsers(@Url String url);

    @GET("login")
    Call<Void> login();

    @Multipart
    @POST("publication/photo")
    Call<Void> uploadMultipleFilesDynamic(
            @Part("idPublication") RequestBody idPublication,
            @Part List<MultipartBody.Part> files);

    //FCM  firebaseCloudMessagingToken
    @POST("firebase/token")
    Call<Void> sendFirebaseToken(@Body String token);

    @POST
    Call<Void> newQuestion(@Body QuestionDTO questionDTO,@Url String url);

    @GET
    Call<List<QuestionDTO>> getQuestions(@Url String url);

    @POST
    Call<Void> newAnswer(@Body QuestionDTO questionDTO,@Url String url);

}