package com.uruguay.obligatoriofrontend.retrofit;

import android.content.Context;

import com.uruguay.obligatoriofrontend.BaseURL;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitAppConfig {

    public static Retrofit getRetrofit(Context context) {

        OkHttpClient okHttpClient = SelfSigningClientBuilder.createClient(context);

        return new Retrofit.Builder()
                .baseUrl(BaseURL.URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();
    }
}