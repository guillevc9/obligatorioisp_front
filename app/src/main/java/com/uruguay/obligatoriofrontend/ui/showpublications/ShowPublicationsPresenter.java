package com.uruguay.obligatoriofrontend.ui.showpublications;

import android.content.Context;

import com.uruguay.obligatoriofrontend.StaticStrings;
import com.uruguay.obligatoriofrontend.dto.PublicationDTO;

import java.util.List;

public class ShowPublicationsPresenter implements IShowPublicationsMVP.Presenter, IShowPublicationsMVP.Interactor.OnFinishedListener {

    private IShowPublicationsMVP.Interactor interactor;
    private IShowPublicationsMVP.View view;
    private Context context;

    public ShowPublicationsPresenter(IShowPublicationsMVP.View view, Context context) {
        this.view = view;
        this.context = context;
        this.interactor = new ShowPublicationsInterceptor(this);
    }

    @Override
    public void showError(String error) {
        view.showError(error);
    }

    @Override
    public Context getContext() {
        return context;
    }

    @Override
    public void getMyPublications() {
        interactor.getMyPublications(this);
    }

    @Override
    public void getMyPublicationsAnnotated() {
        interactor.getMyPublicationsAnnotated(this);
    }

    @Override
    public void onFinished(List<PublicationDTO> publications) {
        view.setPublicationsToRecyclerView(publications);
    }

    @Override
    public void onFailure() {
        view.showError(StaticStrings.READ_PUBLICATIONS_FROM_SERVER_ERROR);
        view.showErrorScreen();
        view.hideLoadingScreen();
    }


}
