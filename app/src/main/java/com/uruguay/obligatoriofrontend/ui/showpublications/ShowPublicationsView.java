package com.uruguay.obligatoriofrontend.ui.showpublications;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.uruguay.obligatoriofrontend.adapter.PublicationAdapter;
import com.uruguay.obligatoriofrontend.StaticStrings;
import com.uruguay.obligatoriofrontend.dto.PublicationDTO;
import com.uruguay.obligatoriofrontend.ui.about.AboutView;
import com.uruguay.obligatoriofrontend.StaticMenu;
import com.uruguay.obligatoriofrontend.R;
import com.uruguay.obligatoriofrontend.ui.home.HomeView;
import com.uruguay.obligatoriofrontend.ui.login.LoginView;
import com.uruguay.obligatoriofrontend.sesion.Session;

import java.util.List;

import static com.uruguay.obligatoriofrontend.StaticStrings.FLAG_MYPUBLICATIONS_OR_ANNOTATED;
import static com.uruguay.obligatoriofrontend.StaticStrings.FLAG_MY_PUBLICATIONS;
import static com.uruguay.obligatoriofrontend.StaticStrings.FLAG_MY_PUBLICATIONS_ANNOTATED;
import static com.uruguay.obligatoriofrontend.StaticStrings.LOG_TAG;

public class ShowPublicationsView extends AppCompatActivity implements IShowPublicationsMVP.View, Session.OnFinishLogin {

    private IShowPublicationsMVP.Presenter presenter;
    private RecyclerView recyclerView;
    private PublicationAdapter adapter;
    private RelativeLayout noDataLayout;
    private RelativeLayout progressBarLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_show_publications);
        setTitle();
        presenter = new ShowPublicationsPresenter(this, this);
        noDataLayout = findViewById(R.id.noDataLayout);
        progressBarLayout = findViewById(R.id.progressBarLayout);
        initializeRecyclerView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        hideErrorScreen();
        showLoadingScreen();
        Session.getInstance().silentSign(this, Session.DEFAULT_CODE, this);
    }

    private void initializeRecyclerView() {
        recyclerView = findViewById(R.id.rvPublications);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(llm);
    }

    @Override
    public void setPublicationsToRecyclerView(List<PublicationDTO> publications) {
        adapter = new PublicationAdapter(publications);
        recyclerView.setAdapter(adapter);
        hideLoadingScreen();
    }

    @Override
    public void showError(String error) {
        Toast.makeText(ShowPublicationsView.this, error, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showMessage(String messaje) {
        Toast.makeText(ShowPublicationsView.this, messaje, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void getMyPublicationsAnnotated() {
        presenter.getMyPublicationsAnnotated();
    }

    @Override
    public void getMyPublications() {
        presenter.getMyPublications();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        if (getIntent().getStringExtra(FLAG_MYPUBLICATIONS_OR_ANNOTATED).equals(FLAG_MY_PUBLICATIONS)) {
            if (menu != null) {
                menu.findItem(R.id.item_my_publications).setVisible(false);
            }
        } else {
            if (menu != null) {
                menu.findItem(R.id.item_publications_annotated).setVisible(false);
            }
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case R.id.item_home:
                intent = new Intent(this, HomeView.class);
                startActivity(intent);
                break;
            case R.id.item_my_publications:
                intent = new Intent(this, ShowPublicationsView.class);
                intent.putExtra(FLAG_MYPUBLICATIONS_OR_ANNOTATED, FLAG_MY_PUBLICATIONS);
                startActivity(intent);

                break;
            case R.id.item_publications_annotated:
                intent = new Intent(this, ShowPublicationsView.class);
                intent.putExtra(FLAG_MYPUBLICATIONS_OR_ANNOTATED, FLAG_MY_PUBLICATIONS_ANNOTATED);
                startActivity(intent);

                break;
            case R.id.item_about:
                startActivity(new Intent(this, AboutView.class));
                break;
            case R.id.item_logout:
                Session.getInstance().getGoogleSignInClient().signOut()
                        .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                Intent i = new Intent(ShowPublicationsView.this, LoginView.class);
                                startActivity(i);
                            }
                        });
                break;
            case R.id.item_exit:
                StaticMenu.exit(this);
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    @Override
    public void showLoadingScreen() {
        Log.d(LOG_TAG, "ShowPublicationsView: showLoadingScreen");
        progressBarLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoadingScreen() {
        Log.d(LOG_TAG, "ShowPublicationsView: hideLoadingScreen");
        progressBarLayout.setVisibility(View.GONE);
    }

    @Override
    public void showErrorScreen() {
        Log.d(LOG_TAG, "ShowPublicationsView: showErrorScreen)");
        noDataLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideErrorScreen() {
        Log.d(LOG_TAG, "ShowPublicationsView: hideErrorScreen)");
        noDataLayout.setVisibility(View.GONE);
    }

    @Override
    public void onFinishedLogin(int resultCode) {
        Log.d(LOG_TAG, "ShowPublicationsView: onFinishedLogin)");
        showLoadingScreen();
        hideErrorScreen();
        update();
    }

    public void update() {
        if (getIntent().getStringExtra(FLAG_MYPUBLICATIONS_OR_ANNOTATED).equals(FLAG_MY_PUBLICATIONS)) {
            getMyPublications();
        } else {
            getMyPublicationsAnnotated();
        }
    }

    public void setTitle() {
        if (getIntent().getStringExtra(FLAG_MYPUBLICATIONS_OR_ANNOTATED).equals(FLAG_MY_PUBLICATIONS)) {
            this.setTitle(StaticStrings.TITLE_MY_PUBLICATIONS_VIEW);
        } else {
            this.setTitle(StaticStrings.TITLE_MY_PUBLICATIONS_ANNOTATED_VIEW);
        }
    }

    @Override
    public void onFailureLogin() {
        Log.d(LOG_TAG, "ShowPublicationsView: onFailureLogin)");
        showErrorScreen();
        hideLoadingScreen();
    }

    public void updateUI(View v) {
        Log.d(LOG_TAG, "ShowPublicationsView: updateUI)");
        hideErrorScreen();
        showLoadingScreen();
        Session.getInstance().silentSign(this, Session.DEFAULT_CODE, this);
    }


}
