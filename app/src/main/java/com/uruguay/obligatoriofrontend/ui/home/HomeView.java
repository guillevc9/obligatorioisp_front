package com.uruguay.obligatoriofrontend.ui.home;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.uruguay.obligatoriofrontend.adapter.PublicationAdapter;
import com.uruguay.obligatoriofrontend.StaticStrings;
import com.uruguay.obligatoriofrontend.dto.CategoryDTO;
import com.uruguay.obligatoriofrontend.dto.ZoneDTO;
import com.uruguay.obligatoriofrontend.ui.about.AboutView;
import com.uruguay.obligatoriofrontend.dto.PublicationDTO;
import com.uruguay.obligatoriofrontend.ui.publication.PublicationView;
import com.uruguay.obligatoriofrontend.StaticMenu;
import com.uruguay.obligatoriofrontend.ui.showpublications.ShowPublicationsView;
import com.uruguay.obligatoriofrontend.R;
import com.uruguay.obligatoriofrontend.ui.login.LoginView;
import com.uruguay.obligatoriofrontend.sesion.Session;

import java.util.List;

import static com.uruguay.obligatoriofrontend.StaticStrings.CONECTION_FAILED;
import static com.uruguay.obligatoriofrontend.StaticStrings.FLAG_MYPUBLICATIONS_OR_ANNOTATED;
import static com.uruguay.obligatoriofrontend.StaticStrings.FLAG_MY_PUBLICATIONS;
import static com.uruguay.obligatoriofrontend.StaticStrings.FLAG_MY_PUBLICATIONS_ANNOTATED;
import static com.uruguay.obligatoriofrontend.StaticStrings.LOG_TAG;

public class HomeView extends AppCompatActivity implements IHomeMVP.View, Session.OnFinishLogin {

    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView recyclerViewPublications;
    private PublicationAdapter adapter;
    private IHomeMVP.Presenter presenter;
    private Spinner spinnerZonesHome;
    private Spinner spinnerCategoriesHome;
    private CheckBox chkCategoriesHome;
    private CheckBox chkZonesHome;
    private RelativeLayout progressBarLayout;
    private RelativeLayout noDataLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_home);
        setTitle(StaticStrings.TITLE_VIEW_HOME);
        initializeAtributes();
    }

    private void initializeAtributes() {
        presenter = new HomePresenter(this, this);
        spinnerZonesHome = findViewById(R.id.spinnerZonesHome);
        spinnerCategoriesHome = findViewById(R.id.spinnerCategoriesHome);
        setListenerToSpinnerCategories();
        setListenerToSpinnerZones();
        chkCategoriesHome = findViewById(R.id.chkCategoriesHome);
        chkZonesHome = findViewById(R.id.chkZonesHome);
        initializeRecycledView();
        progressBarLayout = findViewById(R.id.progressBarLayout);
        noDataLayout = findViewById(R.id.noDataLayout);
        //swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.simpleSwipeRefreshLayout);
        swipeRefreshLayout = findViewById(R.id.simpleSwipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // cancel the Visual indication of a refresh
                swipeRefreshLayout.setRefreshing(false);
                loadData();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        Session.getInstance().silentSign(this, 42, this);
    }

    public void loadData() {
        Log.d(LOG_TAG, "HomeView: cargando LoadData");
        hideErrorScreen();
        showLoadingScreen();
        getCategories();
        getZones();
        getPublications();
    }

    @Override
    public void onFinishedLogin(int resultCode) {
        loadData();
    }

    public void updateUI(View v) {
        hideErrorScreen();
        showLoadingScreen();
        Session.getInstance().silentSign(this, 42, this);
    }

    @Override
    public void onFailureLogin() {
        showError(CONECTION_FAILED);
        showErrorScreen();
    }

    @Override
    public void getPublications() {
        String category;
        if (chkCategoriesHome.isChecked()) {
            spinnerCategoriesHome.setVisibility(CheckBox.VISIBLE);
            category = spinnerCategoriesHome.getSelectedItem().toString();
        } else {
            category = "";
        }
        String zone;
        if (chkZonesHome.isChecked()) {
            zone = spinnerZonesHome.getSelectedItem().toString();
        } else {
            zone = "";
        }
        presenter.getPublications(zone, category);
    }

    @Override
    public void getCategories() {
        presenter.getCategories();
    }

    @Override
    public void getZones() {
        presenter.getZones();
    }

    @Override
    public void showLoadingScreen() {
        Log.d(LOG_TAG, "HomeView: showLoadingScreen");
        progressBarLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoadingScreen() {
        Log.d(LOG_TAG, "HomeView: hideLoadingScreen");
        progressBarLayout.setVisibility(View.GONE);
    }

    @Override
    public void showErrorScreen() {
        Log.d(LOG_TAG, "HomeView: showErrorScreen)");
        noDataLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideErrorScreen() {
        Log.d(LOG_TAG, "HomeView: hideErrorScreen)");
        noDataLayout.setVisibility(View.GONE);
    }

    @Override
    public void setPublicationsToRecyclerView(List<PublicationDTO> publications) {
        adapter = new PublicationAdapter(publications);
        recyclerViewPublications.setAdapter(adapter);
    }

    @Override
    public void setZonesToSpinner() {
        List<ZoneDTO> zones = Session.getInstance().getZones();
        spinnerZonesHome.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, zones));
    }

    public void onClickChkZones(View view) {
        if (chkZonesHome.isChecked()) {
            spinnerZonesHome.setVisibility(CheckBox.VISIBLE);
        } else {
            spinnerZonesHome.setVisibility(CheckBox.INVISIBLE);
        }
        Log.d(LOG_TAG, "HomeView: Actualizar RecyclerView por Zones");
        getPublications();
    }

    public void onClickChkCategories(View view) {
        if (chkCategoriesHome.isChecked()) {
            spinnerCategoriesHome.setVisibility(CheckBox.VISIBLE);
        } else {
            spinnerCategoriesHome.setVisibility(CheckBox.INVISIBLE);
        }
        Log.d(LOG_TAG, "HomeView: Actualizar RecyclerView por Categories");
        getPublications();
    }

    @Override
    public void setCategoriesToSpinner() {
        List<CategoryDTO> categories = Session.getInstance().getCategories();
        spinnerCategoriesHome.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, categories));
    }

    @Override
    public void showError(String error) {
        Toast.makeText(HomeView.this, error, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showMessage(String messaje) {
        Toast.makeText(HomeView.this, messaje, Toast.LENGTH_SHORT).show();
    }

    public void onClickFloatBtn(View v) {
        Log.d(LOG_TAG, "HomeView: Click en FloatBtn, nueva publicacion");
        Intent i = new Intent(this, PublicationView.class);
        startActivity(i);
    }

    private void initializeRecycledView() {
        recyclerViewPublications = findViewById(R.id.recyclerV);
        recyclerViewPublications.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        recyclerViewPublications.setLayoutManager(llm);
    }

    private void setTitle(String title) {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(title);
        }
    }

    private void setListenerToSpinnerCategories() {
        spinnerCategoriesHome.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                Log.d(LOG_TAG, "HomeView: setListenerToSpinnerCategories");
                getPublications();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                Log.d(LOG_TAG, "HomeView: onNothingSelectedCategories");
            }
        });
    }

    private void setListenerToSpinnerZones() {
        spinnerZonesHome.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                Log.d(LOG_TAG, "HomeView: setListenerToSpinnerZones");
                getPublications();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                Log.d(LOG_TAG, "HomeView: onNothingSelectedZones");
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        if(menu != null){
            menu.findItem(R.id.item_home).setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case R.id.item_my_publications:
                intent = new Intent(this, ShowPublicationsView.class);
                intent.putExtra(FLAG_MYPUBLICATIONS_OR_ANNOTATED, FLAG_MY_PUBLICATIONS);
                startActivity(intent);
                break;
            case R.id.item_publications_annotated:
                intent = new Intent(this, ShowPublicationsView.class);
                intent.putExtra(FLAG_MYPUBLICATIONS_OR_ANNOTATED, FLAG_MY_PUBLICATIONS_ANNOTATED);
                startActivity(intent);

                break;
            case R.id.item_about:
                startActivity(new Intent(this, AboutView.class));
                break;
            case R.id.item_logout:
                Session.getInstance().getGoogleSignInClient().signOut()
                        .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                Intent i = new Intent(HomeView.this, LoginView.class);
                                startActivity(i);
                            }
                        });
                break;
            case R.id.item_exit:
                StaticMenu.exit(this);
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        StaticMenu.exit(this);
    }
}
