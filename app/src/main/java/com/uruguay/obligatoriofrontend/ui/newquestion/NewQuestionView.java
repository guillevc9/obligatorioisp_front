package com.uruguay.obligatoriofrontend.ui.newquestion;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.uruguay.obligatoriofrontend.R;
import com.uruguay.obligatoriofrontend.dto.QuestionDTO;

import static com.uruguay.obligatoriofrontend.StaticStrings.ID;
import static com.uruguay.obligatoriofrontend.StaticStrings.LOG_TAG;
import static com.uruguay.obligatoriofrontend.StaticStrings.NEW_QUESTION_EMPTY;
import static com.uruguay.obligatoriofrontend.StaticStrings.TITLE_NEW_QUESTION_VIEW;

public class NewQuestionView extends AppCompatActivity implements INewQuestionMVP.View {

    private static final int MAX_LENGTH_QUESTION = 140;
    private EditText etQuestion;
    private int idPublication;
    private INewQuestionMVP.Presenter presenter;
    private RelativeLayout noDataLayout;
    private RelativeLayout progressBarLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_new_question);
        setTitle(TITLE_NEW_QUESTION_VIEW);
        initialize();
    }

    private void initialize() {
        etQuestion = findViewById(R.id.etQuestion);
        idPublication = getIntent().getIntExtra(ID, 0);
        presenter = new NewQuestionPresenter(this, this);
        noDataLayout = findViewById(R.id.noDataLayout);
        progressBarLayout = findViewById(R.id.progressBarLayout);
    }

    public void onSendNewQuestion(View view) {
        Log.d(LOG_TAG, "NewQuestionView: onSendNewQuestion");
        String question = etQuestion.getText().toString();
        if (question.isEmpty()) {
            showError(NEW_QUESTION_EMPTY);
        } else if (question.length() > MAX_LENGTH_QUESTION) {
            String error = "La pregunta no puede tener más de " + MAX_LENGTH_QUESTION + " caracteres";
            showError(error);
        } else {
            showLoadingScreen();
            QuestionDTO questionDTO = new QuestionDTO();
            questionDTO.setQuestion(question);
            presenter.newQuestion(idPublication, questionDTO);
        }
    }

    public void onCancelNewQuestion(View view) {
        finish();
    }

    public void finishActivity() {
        finish();
    }

    @Override
    public void showLoadingScreen() {
        Log.d(LOG_TAG, "ShowPublicationDetailsView: showLoadingScreen");
        progressBarLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoadingScreen() {
        Log.d(LOG_TAG, "ShowPublicationDetailsView: hideLoadingScreen");
        progressBarLayout.setVisibility(View.GONE);
    }

    @Override
    public void showErrorScreen() {
        Log.d(LOG_TAG, "ShowPublicationDetailsView: showErrorScreen)");
        noDataLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideErrorScreen() {
        Log.d(LOG_TAG, "ShowPublicationDetailsView: hideErrorScreen)");
        noDataLayout.setVisibility(View.GONE);
    }

    @Override
    public void showError(String error) {
        Toast.makeText(NewQuestionView.this, error, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(NewQuestionView.this, message, Toast.LENGTH_LONG).show();
    }

}
