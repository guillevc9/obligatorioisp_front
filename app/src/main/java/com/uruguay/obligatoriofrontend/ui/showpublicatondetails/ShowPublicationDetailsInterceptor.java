package com.uruguay.obligatoriofrontend.ui.showpublicatondetails;


import android.util.Log;

import com.google.gson.JsonPrimitive;
import com.uruguay.obligatoriofrontend.HttpCodes;
import com.uruguay.obligatoriofrontend.StaticStrings;
import com.uruguay.obligatoriofrontend.dto.QuestionDTO;
import com.uruguay.obligatoriofrontend.retrofit.IRetrofitApiConfigMVP;
import com.uruguay.obligatoriofrontend.retrofit.RetrofitAppConfig;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.uruguay.obligatoriofrontend.StaticStrings.LOG_TAG;

public class ShowPublicationDetailsInterceptor implements IShowPublicationDetails.Interactor {

    private IShowPublicationDetails.Presenter presenter;

    public ShowPublicationDetailsInterceptor(IShowPublicationDetails.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void getQuestions(int idPublication, final OnFinishedListenerQuestions listener) {
        IRetrofitApiConfigMVP getResponse = RetrofitAppConfig.getRetrofit(presenter.getContext()).create(IRetrofitApiConfigMVP.class);
        String url = String.format("publication/%s/questions", idPublication);
        Log.d(LOG_TAG, "ShowPublicationDetailsInterceptor: getQuestions URL " + url);
        Call<List<QuestionDTO>> call = getResponse.getQuestions(url);
        call.enqueue(new Callback<List<QuestionDTO>>() {

            @Override
            public void onResponse(Call<List<QuestionDTO>> call, Response<List<QuestionDTO>> response) {
                Log.d(LOG_TAG, "ShowPublicationDetailsInterceptor: getQuestions response.code(): " + response.code());
                if (response.code() == HttpCodes.OK) {
                    Log.d(LOG_TAG, "ShowPublicationDetailsInterceptor: getQuestions onResponse OK " + response.body().size());
                    listener.onFinishedQuestions(response.body());
                } else if (response.code() == HttpCodes.UNAUTHORIZED) {
                    presenter.showError(StaticStrings.UNAUTHORIZED);
                } else {
                    Log.d(LOG_TAG, "ShowPublicationDetailsInterceptor: getQuestions onResponse ERROR");
                }
            }

            @Override
            public void onFailure(Call<List<QuestionDTO>> call, Throwable t) {
                Log.d(LOG_TAG, "ShowPublicationDetailsInterceptor: getQuestions onFailure");
                listener.onFailureQuestions();
            }
        });
    }

    @Override
    public void isAnnotated(final int id, final OnFinishedListener onFinishedListener) {
        IRetrofitApiConfigMVP getResponse = RetrofitAppConfig.getRetrofit(presenter.getContext()).create(IRetrofitApiConfigMVP.class);
        String url = String.format("publication/annotated/%s", id);
        Call<JsonPrimitive> call = getResponse.getAnnotated(url);
        call.enqueue(new Callback<JsonPrimitive>() {

            @Override
            public void onResponse(Call<JsonPrimitive> call, Response<JsonPrimitive> response) {
                if (response.code() == HttpCodes.OK) {
                    boolean aux = response.body().getAsBoolean();
                    Log.d(LOG_TAG, "ShowPublicationDetailsInterceptor: isAnnotated OK: " + aux);
                    presenter.annotated(aux);
                } else if (response.code() == HttpCodes.UNAUTHORIZED) {
                    presenter.showError(StaticStrings.UNAUTHORIZED);
                }
            }

            @Override
            public void onFailure(Call<JsonPrimitive> call, Throwable t) {
                Log.d(LOG_TAG, "ShowPublicationDetailsInterceptor: isAnnotated onFailureLogin");
                onFinishedListener.onFailure();
            }
        });
    }

    @Override
    public void register(int id, final OnFinishedListener onFinishedListener) {
        Log.d(LOG_TAG, "ShowPublicationDetailsInterceptor: quiero register");
        IRetrofitApiConfigMVP getResponse = RetrofitAppConfig.getRetrofit(presenter.getContext()).create(IRetrofitApiConfigMVP.class);
        String url = String.format("publication/%s/annotate/", id);
        Call<JsonPrimitive> call = getResponse.getAnnotated(url);
        call.enqueue(new Callback<JsonPrimitive>() {

            @Override
            public void onResponse(Call<JsonPrimitive> call, Response<JsonPrimitive> response) {
                boolean aux;
                if (response.code() == HttpCodes.OK) {
                    aux = response.body().getAsBoolean();
                    Log.d(LOG_TAG, "ShowPublicationDetailsInterceptor: register OK: " + aux);
                    presenter.annotated(aux);
                    onFinishedListener.onFinished();
                } else if (response.code() == HttpCodes.UNAUTHORIZED) {
                    presenter.showError(StaticStrings.UNAUTHORIZED);
                } else {
                    Log.d(LOG_TAG, "ShowPublicationDetailsInterceptor: register else");
                    presenter.annotated(false);
                }
            }

            @Override
            public void onFailure(Call<JsonPrimitive> call, Throwable t) {
                Log.d(LOG_TAG, "ShowPublicationDetailsInterceptor: register onFailureLogin");
                presenter.annotated(false);
                onFinishedListener.onFailure();
            }
        });
    }

    @Override
    public void rescind(final int id, final OnFinishedListener onFinishedListener) {
        Log.d(LOG_TAG, "ShowPublicationDetailsInterceptor: quiero rescind");
        IRetrofitApiConfigMVP getResponse = RetrofitAppConfig.getRetrofit(presenter.getContext()).create(IRetrofitApiConfigMVP.class);
        String url = String.format("publication/%s/discard/", id);
        Call<JsonPrimitive> call = getResponse.getAnnotated(url);
        call.enqueue(new Callback<JsonPrimitive>() {

            @Override
            public void onResponse(Call<JsonPrimitive> call, Response<JsonPrimitive> response) {
                if (response.code() == HttpCodes.OK && response.body().getAsBoolean()) {
                    presenter.annotated(false);
                    Log.d(LOG_TAG, "ShowPublicationDetailsInterceptor: rescind OK");
                    onFinishedListener.onFinished();
                } else if (response.code() == HttpCodes.UNAUTHORIZED) {
                    presenter.showError(StaticStrings.UNAUTHORIZED);
                } else {
                    Log.d(LOG_TAG, "ShowPublicationDetailsInterceptor: rescind else");
                    presenter.annotated(true);
                }
            }

            @Override
            public void onFailure(Call<JsonPrimitive> call, Throwable t) {
                presenter.annotated(true);
                Log.d(LOG_TAG, "ShowPublicationDetailsInterceptor: rescind ERROR");
                onFinishedListener.onFailure();
            }
        });
    }

}
