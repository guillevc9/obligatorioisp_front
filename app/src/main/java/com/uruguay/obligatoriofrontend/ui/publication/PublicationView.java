package com.uruguay.obligatoriofrontend.ui.publication;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.squareup.picasso.Picasso;
import com.uruguay.obligatoriofrontend.BaseURL;
import com.uruguay.obligatoriofrontend.StaticMenu;
import com.uruguay.obligatoriofrontend.OperationResult;
import com.uruguay.obligatoriofrontend.R;
import com.uruguay.obligatoriofrontend.StaticStrings;
import com.uruguay.obligatoriofrontend.picasso.PicassoAppConfig;
import com.uruguay.obligatoriofrontend.ui.about.AboutView;
import com.uruguay.obligatoriofrontend.dto.PublicationDTO;
import com.uruguay.obligatoriofrontend.ui.home.HomeView;
import com.uruguay.obligatoriofrontend.ui.login.LoginView;
import com.uruguay.obligatoriofrontend.sesion.Session;
import com.uruguay.obligatoriofrontend.ui.showImage.showImageView;
import com.uruguay.obligatoriofrontend.ui.showpublications.ShowPublicationsView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static com.uruguay.obligatoriofrontend.StaticStrings.ADDRESS;
import static com.uruguay.obligatoriofrontend.StaticStrings.BTN_MODIFY_PUBLICATION;
import static com.uruguay.obligatoriofrontend.StaticStrings.CATEGORY;
import static com.uruguay.obligatoriofrontend.StaticStrings.DESCRIPTION;
import static com.uruguay.obligatoriofrontend.StaticStrings.FLAG_MYPUBLICATIONS_OR_ANNOTATED;
import static com.uruguay.obligatoriofrontend.StaticStrings.FLAG_MY_PUBLICATIONS;
import static com.uruguay.obligatoriofrontend.StaticStrings.FLAG_MY_PUBLICATIONS_ANNOTATED;
import static com.uruguay.obligatoriofrontend.StaticStrings.GPS_NOT_LOCATION;
import static com.uruguay.obligatoriofrontend.StaticStrings.GPS_OFF;
import static com.uruguay.obligatoriofrontend.StaticStrings.ID;
import static com.uruguay.obligatoriofrontend.StaticStrings.IMAGE;
import static com.uruguay.obligatoriofrontend.StaticStrings.LOG_TAG;
import static com.uruguay.obligatoriofrontend.StaticStrings.PHOTOS;
import static com.uruguay.obligatoriofrontend.StaticStrings.TITLE;
import static com.uruguay.obligatoriofrontend.StaticStrings.TITLE_MODIFY_PUBLICATION_VIEW;
import static com.uruguay.obligatoriofrontend.StaticStrings.ZONE;

public class PublicationView extends AppCompatActivity implements IPublicationMVP.View, Session.OnFinishLogin, LocationListener {

    private TextView title;
    private EditText etTitle;
    private EditText etDescription;
    private EditText etAddress;
    private Spinner spinnerCategories;
    private Spinner spinnerZones;
    private IPublicationMVP.Presenter presenter;
    private boolean update;
    private Button btnSend;
    private static int REQUEST_IMAGE_CAPTURE_1 = 678;
    private static int REQUEST_IMAGE_CAPTURE_2 = 679;
    private static int REQUEST_IMAGE_CAPTURE_3 = 680;
    private static final int REQUEST_GPS_PERMISSION = 42;
    private List<String> imagePaths;
    private boolean picture1;
    private boolean picture2;
    private boolean picture3;
    private boolean picture1Local;
    private boolean picture2Local;
    private boolean picture3Local;
    private String picture1Path;
    private String picture2Path;
    private String picture3Path;
    private String picturePath;
    private ImageView imgView1;
    private ImageView imgView2;
    private ImageView imgView3;
    private int photos;
    private int id;
    private Picasso picasso;
    private RelativeLayout noDataLayout;
    private RelativeLayout progressBarLayout;
    private LocationManager locationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_publication);
        initialize();
    }

    private void initialize() {
        this.title = findViewById(R.id.tvTitle);
        this.noDataLayout = findViewById(R.id.noDataLayout);
        this.progressBarLayout = findViewById(R.id.progressBarLayout);
        this.etTitle = findViewById(R.id.etTitle);
        this.etDescription = findViewById(R.id.etDescription);
        this.etAddress = findViewById(R.id.etAddress);
        this.spinnerCategories = findViewById(R.id.spinnerCategories);
        this.spinnerZones = findViewById(R.id.spinnerZones);
        this.presenter = new PublicationPresenter(this, this);
        this.update = false;
        this.setTitle(StaticStrings.TITLE_VIEW_NEW_PUBLICATION);
        this.btnSend = findViewById(R.id.btnSend);
        this.spinnerCategories.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, Session.getInstance().getCategories()));
        this.spinnerZones.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, Session.getInstance().getZones()));
        this.photos = 0;
        this.id = 0;
        this.imagePaths = new ArrayList<>();
        this.picture1 = false;
        this.picture2 = false;
        this.picture3 = false;
        this.picturePath = "";
        this.picture1Local = false;
        this.picture2Local = false;
        this.picture3Local = false;
        this.picture1Path = "";
        this.picture2Path = "";
        this.picture3Path = "";
        this.imgView1 = findViewById(R.id.imgView1);
        this.imgView2 = findViewById(R.id.imgView2);
        this.imgView3 = findViewById(R.id.imgView3);
        this.picasso = PicassoAppConfig.getPicasso(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        hideErrorScreen();
        showLoadingScreen();
        Session.getInstance().silentSign(this, Session.DEFAULT_CODE, this);
    }

    @Override
    public void onFinishedLogin(int resultCode) {
        Log.d(LOG_TAG, "NewPublicationsView: onFinishedLogin)");
        showLoadingScreen();
        hideErrorScreen();
        checkUpdate();
    }

    @Override
    public void onFailureLogin() {
        Log.d(LOG_TAG, "NewPublicationsView: onFailureLogin)");
        showErrorScreen();
        hideLoadingScreen();
    }

    private void checkUpdate() {
        Intent intent = getIntent();
        update = intent.getBooleanExtra(StaticStrings.FLAG_UPDATE_PUBLICATION, false);
        if (update) {
            updatePublication();
        } else {
            hideLoadingScreen();
        }
    }

    private void updatePublication() {
        loadData();
        hideLoadingScreen();
        btnSend.setText(BTN_MODIFY_PUBLICATION);
        setTitle(TITLE_MODIFY_PUBLICATION_VIEW);
        title.setText(TITLE_MODIFY_PUBLICATION_VIEW);
    }

    private void loadData() {
        Intent intent = getIntent();
        this.etTitle.setText(intent.getStringExtra(TITLE));
        this.etDescription.setText(intent.getStringExtra(DESCRIPTION));
        this.etAddress.setText(intent.getStringExtra(ADDRESS));
        loadSpinnerCategories(intent);
        loadSpinnerZones(intent);
        this.id = getIntent().getIntExtra(ID, 0);
        this.photos = getIntent().getIntExtra(PHOTOS, 0);
        Log.d(LOG_TAG, "NewPublicationsView: loadData photos: " + photos);
        loadImages();
    }

    private void loadSpinnerZones(Intent intent) {
        int count;
        int position;
        String aux;
        count = spinnerZones.getAdapter().getCount();
        position = 0;
        aux = intent.getStringExtra(ZONE);
        for (int i = 0; i < count; i++) {
            if (aux.equals(spinnerZones.getAdapter().getItem(i).toString())) {
                position = i;
            }
        }
        spinnerZones.setSelection(position);
    }

    private void loadSpinnerCategories(Intent intent) {
        int count = spinnerCategories.getAdapter().getCount();
        int position = 0;
        String aux = intent.getStringExtra(CATEGORY);
        for (int i = 0; i < count; i++) {
            if (aux.equals(spinnerCategories.getAdapter().getItem(i).toString())) {
                position = i;
            }
        }
        spinnerCategories.setSelection(position);
    }

    private void loadImages() {
        if (photos != 0) {
            for (int i = 1; i <= photos; i++) {
                String url = BaseURL.URL + "publication/" + id + "/downloadPhoto/" + i;
                loadImage(i, url);
            }
        }
    }

    private void loadImage(int i, String url) {
        switch (i) {
            case 1:
                picasso.load(url).into(imgView1);
                picture1 = true;
                picture1Local = false;
                break;
            case 2:
                picasso.load(url).into(imgView2);
                picture2 = true;
                picture2Local = false;
                break;
            case 3:
                picasso.load(url).into(imgView3);
                picture3 = true;
                picture3Local = false;
                break;
        }
    }

    private void setTitle(String title) {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(title);
        }
    }

    public void clicPicture(View view) {
        if (picture1) {
            Intent intent = new Intent(this, showImageView.class);
            String path;
            if (picture1Local) {
                path = picture1Path;
                intent.putExtra(StaticStrings.FLAG_LOCAL, true);
            } else {
                path = BaseURL.URL + "publication/" + id + "/downloadPhoto/1";
                intent.putExtra(StaticStrings.FLAG_LOCAL, false);
            }
            intent.putExtra(IMAGE, path);
            startActivity(intent);
        } else {
            dispatchTakePictureIntent(REQUEST_IMAGE_CAPTURE_1);
        }
    }

    public void clicPicture2(View view) {
        if (picture2) {
            Intent intent = new Intent(this, showImageView.class);
            String path;
            if (picture2Local) {
                path = picture2Path;
                intent.putExtra(StaticStrings.FLAG_LOCAL, true);
            } else {
                path = BaseURL.URL + "publication/" + id + "/downloadPhoto/2";
                intent.putExtra(StaticStrings.FLAG_LOCAL, false);
            }
            intent.putExtra(IMAGE, path);
            startActivity(intent);
        } else {
            dispatchTakePictureIntent(REQUEST_IMAGE_CAPTURE_2);
        }
    }

    public void clicPicture3(View view) {
        if (picture3) {
            Intent intent = new Intent(this, showImageView.class);
            String path;
            if (picture3Local) {
                path = picture3Path;
                intent.putExtra(StaticStrings.FLAG_LOCAL, true);
            } else {
                path = BaseURL.URL + "publication/" + id + "/downloadPhoto/3";
                intent.putExtra(StaticStrings.FLAG_LOCAL, false);
            }
            intent.putExtra(IMAGE, path);
            startActivity(intent);
        } else {
            dispatchTakePictureIntent(REQUEST_IMAGE_CAPTURE_3);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE_1 && resultCode == RESULT_OK) {
            File file_picture1 = new File(picturePath);
            File reducePicture = saveBitmapToFile(file_picture1);
            picture1Path = reducePicture.getAbsolutePath();
            imagePaths.add(picture1Path);
            Bitmap bitmap = BitmapFactory.decodeFile(picture1Path);
            imgView1.setImageBitmap(bitmap);
            picture1 = true;
            picture1Local = true;
            file_picture1.deleteOnExit();
            reducePicture.deleteOnExit();
        }
        if (requestCode == REQUEST_IMAGE_CAPTURE_2 && resultCode == RESULT_OK) {
            File file_picture2 = new File(picturePath);
            File reducePicture = saveBitmapToFile(file_picture2);
            picture2Path = reducePicture.getAbsolutePath();
            imagePaths.add(picture2Path);
            Bitmap bitmap = BitmapFactory.decodeFile(picture2Path);
            imgView2.setImageBitmap(bitmap);
            picture2 = true;
            picture2Local = true;
            file_picture2.deleteOnExit();
            reducePicture.deleteOnExit();
        }
        if (requestCode == REQUEST_IMAGE_CAPTURE_3 && resultCode == RESULT_OK) {
            File file_picture3 = new File(picturePath);
            File reducePicture = saveBitmapToFile(file_picture3);
            picture3Path = reducePicture.getAbsolutePath();
            imagePaths.add(picture3Path);
            Bitmap bitmap = BitmapFactory.decodeFile(picture3Path);
            imgView3.setImageBitmap(bitmap);
            picture3 = true;
            picture3Local = true;
            file_picture3.deleteOnExit();
            reducePicture.deleteOnExit();
        }
    }

    //Source: https://stackoverflow.com/questions/18573774/how-to-reduce-an-image-file-size-before-uploading-to-a-server
    public File saveBitmapToFile(File file) {
        try {
            // BitmapFactory options to downsize the image
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            o.inSampleSize = 6;
            // factor of downsizing the image
            FileInputStream inputStream = new FileInputStream(file);
            //Bitmap selectedBitmap = null;
            BitmapFactory.decodeStream(inputStream, null, o);
            inputStream.close();
            // The new size we want to scale to
            final int REQUIRED_SIZE = 75;
            // Find the correct scale value. It should be the power of 2.
            int scale = 1;
            while (o.outWidth / scale / 2 >= REQUIRED_SIZE && o.outHeight / scale / 2 >= REQUIRED_SIZE) {
                scale *= 2;
            }
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            inputStream = new FileInputStream(file);
            Bitmap selectedBitmap = BitmapFactory.decodeStream(inputStream, null, o2);
            inputStream.close();
            // here i override the original image file
            file.createNewFile();
            FileOutputStream outputStream = new FileOutputStream(file);
            selectedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
            return file;
        } catch (Exception e) {
            return null;
        }
    }

    private File createImageFile() throws IOException {
        String imageFileName = "PNG_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(imageFileName, ".png", storageDir);
        return image;
    }

    private void dispatchTakePictureIntent(int requestCode) {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {

            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                try {
                    Log.d(LOG_TAG, "NewPublicationViewImp: " + this.getPackageName());
                    picturePath = photoFile.getAbsolutePath();
                    Uri photoURI = FileProvider.getUriForFile(this, "com.uruguay.obligatoriofrontend.provider", photoFile);
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                    startActivityForResult(takePictureIntent, requestCode);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void saveImage(Bitmap imageBitmap, int index) {
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = null;
        String filename = "image" + index;
        FileOutputStream out = null;
        try {
            image = File.createTempFile(filename, ".png", storageDir);
            out = new FileOutputStream(image.getAbsolutePath());
            imageBitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
            String absolutePath = image.getAbsolutePath();
            switch (index) {
                case 1:
                    picture1Path = absolutePath;
                    break;
                case 2:
                    picture2Path = absolutePath;
                    break;
                case 3:
                    picture3Path = absolutePath;
                    break;
            }
            imagePaths.add(absolutePath);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void showError(String error) {
        Toast.makeText(PublicationView.this, error, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showMessage(String message) {
        hideLoadingScreen();
        Toast.makeText(PublicationView.this, message, Toast.LENGTH_SHORT).show();
    }

    public void onClickSend(View v) {
        PublicationDTO publication = getPublicationDTO();
        OperationResult operationResult = PublicationValidator.validate(publication);
        if (operationResult.isValid) {
            showLoadingScreen();
            Log.d(LOG_TAG, "NewPublicationView: click Publicar");
            if (update) {
                sendUpdatePublication(publication);
                Log.d(LOG_TAG, "sendUpdatePublication");
            } else {
                sendNewPublication(publication);
                Log.d(LOG_TAG, "sendNewPublication");
            }
        } else {
            showError(operationResult.message);
        }
    }

    @Override
    public void sendNewPublication(PublicationDTO publication) {
        presenter.sendNewPublication(publication, imagePaths);
    }

    @Override
    public void sendUpdatePublication(PublicationDTO publication) {
        publication.setId(getIntent().getIntExtra(ID, 0));
        presenter.sendUpdatePublication(publication, imagePaths);
    }

    @Override
    public void finishActivity() {
        finish();
    }

    private PublicationDTO getPublicationDTO() {
        String title = etTitle.getText().toString();
        String description = etDescription.getText().toString();
        String address = etAddress.getText().toString();
        String category = spinnerCategories.getSelectedItem().toString();
        String zone = spinnerZones.getSelectedItem().toString();
        String userGoogleID = Session.getInstance().getUserGoogleId();
        String userName = Session.getInstance().getName();
        String userFamilyName = Session.getInstance().getFamilyName();
        return new PublicationDTO(0, title, description, address, category, zone, userGoogleID, userName, userFamilyName, photos + imagePaths.size());
    }

    @Override
    public void emptyFields() {
        etTitle.setText("");
        etDescription.setText("");
        etAddress.setText("");
        imgView1.setImageDrawable(null);
        imgView2.setImageDrawable(null);
        imgView3.setImageDrawable(null);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case R.id.item_home:
                intent = new Intent(this, HomeView.class);
                startActivity(intent);
                break;
            case R.id.item_my_publications:
                intent = new Intent(this, ShowPublicationsView.class);
                intent.putExtra(FLAG_MYPUBLICATIONS_OR_ANNOTATED, FLAG_MY_PUBLICATIONS);
                startActivity(intent);

                break;
            case R.id.item_publications_annotated:
                intent = new Intent(this, ShowPublicationsView.class);
                intent.putExtra(FLAG_MYPUBLICATIONS_OR_ANNOTATED, FLAG_MY_PUBLICATIONS_ANNOTATED);
                startActivity(intent);

                break;
            case R.id.item_about:
                startActivity(new Intent(this, AboutView.class));
                break;
            case R.id.item_logout:
                Session.getInstance().getGoogleSignInClient().signOut()
                        .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                Intent i = new Intent(PublicationView.this, LoginView.class);
                                startActivity(i);
                            }
                        });
                break;
            case R.id.item_exit:
                StaticMenu.exit(this);
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    @Override
    public void showLoadingScreen() {
        Log.d(LOG_TAG, "NewPublicationsView: showLoadingScreen");
        progressBarLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoadingScreen() {
        Log.d(LOG_TAG, "NewPublicationsView: hideLoadingScreen");
        progressBarLayout.setVisibility(View.GONE);
    }

    @Override
    public void showErrorScreen() {
        Log.d(LOG_TAG, "NewPublicationsView: showErrorScreen)");
        noDataLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideErrorScreen() {
        Log.d(LOG_TAG, "NewPublicationsView: hideErrorScreen)");
        noDataLayout.setVisibility(View.GONE);
    }

    public void updateUI(View v) {
        Log.d(LOG_TAG, "NewPublicationsView: updateUI)");
        hideErrorScreen();
        showLoadingScreen();
        Session.getInstance().silentSign(this, Session.DEFAULT_CODE, this);
    }

    public void onClickGetLocation(View v) {
        Log.d(LOG_TAG, "NewPublicationsView: onClickGetLocation");
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Log.d(LOG_TAG, "NewPublicationsView: onClickGetLocation: Permission not granted");
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_GPS_PERMISSION);
        } else {
            Log.d(LOG_TAG, "NewPublicationsView: onClickGetLocation: Permission granted");
            final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                Log.d(LOG_TAG, "NewPublicationsView: onClickGetLocation: GPS NO ENCENDIDO");
                showMessage(GPS_OFF);
            } else {
                Log.d(LOG_TAG, "NewPublicationsView: onClickGetLocation: GPS ENCENDIDO");
                getAddress();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_GPS_PERMISSION: {
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    Log.d(LOG_TAG, "NewPublicationsView: onClickGetLocation: Permission not granted");
                } else {
                    Log.d(LOG_TAG, "NewPublicationsView: onClickGetLocation: Permission granted");
                    final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                    if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                        Log.d(LOG_TAG, "NewPublicationsView: onClickGetLocation: GPS NO ENCENDIDO");
                        showMessage(GPS_OFF);
                    } else {
                        Log.d(LOG_TAG, "NewPublicationsView: onClickGetLocation: GPS ENCENDIDO");
                        getAddress();
                    }
                }
            }
            return;
        }
    }

    public void getAddress() {
        LocationManager lm = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;

        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
        }

        if (!gps_enabled) {
            showMessage(GPS_OFF);
        } else if (gps_enabled) {
            Toast.makeText(this, "Obteniendo datos del GPS, por favor espere", Toast.LENGTH_LONG).show();
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                Log.d(LOG_TAG, "NewPublicationsView: getAddress: Permission granted");
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
            }
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d(LOG_TAG, "NewPublicationsView: onLocationChanged: Latitude" + location.getLatitude() + ", Longitude: " + location.getLongitude());
        Geocoder geocoder;
        int index = 0;
        List<Address> addresses;
        geocoder = new Geocoder(this, Locale.getDefault());
        try {
            if (location != null) {
                addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                if (!addresses.isEmpty()) {
                    String address_style = addresses.get(index).getThoroughfare() + " " + addresses.get(index).getSubThoroughfare();
                    Log.d(LOG_TAG, "NewPublicationsView: getAddress: Address: " + address_style);
                    etAddress.setText(address_style);
                }
            } else {
                showMessage(GPS_NOT_LOCATION);
            }
        } catch (IOException e) {
            Log.d(LOG_TAG, "NewPublicationsView: getAddress: IOException");
            e.printStackTrace();
        }
        locationManager.removeUpdates(this);
    }

    @Override
    public void onProviderDisabled(String provider) {
        Log.d(LOG_TAG, "NewPublicationsView: onProviderDisabled");
        Log.d("Latitude", "disable");
    }

    @Override
    public void onProviderEnabled(String provider) {
        Log.d(LOG_TAG, "NewPublicationsView: onProviderEnabled");
        Log.d("Latitude", "enable");
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        Log.d(LOG_TAG, "NewPublicationsView: onStatusChanged: " + status);
        Log.d("Latitude", "status");
    }

}
