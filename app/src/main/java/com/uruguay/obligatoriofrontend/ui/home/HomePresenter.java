package com.uruguay.obligatoriofrontend.ui.home;

import android.content.Context;

import com.uruguay.obligatoriofrontend.StaticStrings;
import com.uruguay.obligatoriofrontend.dto.PublicationDTO;

import java.util.List;

public class HomePresenter implements IHomeMVP.Presenter, IHomeMVP.Interactor.OnFinishedListener {

    private Context context;
    private IHomeMVP.View view;
    private IHomeMVP.Interactor interactor;

    public HomePresenter(Context context, IHomeMVP.View view) {
        this.context = context;
        this.view = view;
        this.interactor = new HomeInteractor(this);
    }

    @Override
    public void getPublications(String zone,String category) {
        interactor.getPublications(zone,category,this);
    }

    @Override
    public void getCategories() {
        interactor.getCategories();
    }

    @Override
    public void getZones() {
        interactor.getZones();
    }

    @Override
    public void showError(String error) {
        view.showError(error);
    }

    @Override
    public Context getContext() {
        return context;
    }

    @Override
    public void setZonesToSpinner() {
        view.setZonesToSpinner();
    }

    @Override
    public void setCategoriesToSpinner() {
        view.setCategoriesToSpinner();
    }

    @Override
    public void onFinished(List<PublicationDTO> publications) {
        view.setPublicationsToRecyclerView(publications);
        view.hideLoadingScreen();
    }

    @Override
    public void onFailure() {
        view.showError(StaticStrings.READ_PUBLICATIONS_FROM_SERVER_ERROR);
        view.hideLoadingScreen();
        view.showErrorScreen();
    }

}
