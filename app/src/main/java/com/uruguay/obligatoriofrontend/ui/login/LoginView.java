package com.uruguay.obligatoriofrontend.ui.login;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;

import com.google.android.gms.common.SignInButton;
import com.uruguay.obligatoriofrontend.R;
import com.uruguay.obligatoriofrontend.StaticStrings;
import com.uruguay.obligatoriofrontend.sesion.Session;
import com.uruguay.obligatoriofrontend.ui.home.HomeView;

import static com.uruguay.obligatoriofrontend.StaticStrings.LOG_TAG;
import static com.uruguay.obligatoriofrontend.StaticStrings.TITLE_LOGIN_VIEW;

public class LoginView extends AppCompatActivity implements ILoginMVP.View, Session.OnFinishLogin {

    private ILoginMVP.Presenter presenter;
    private RelativeLayout noDataLayout;
    private RelativeLayout progressBarLayout;
    private SignInButton signInButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_login);
        setTitle(TITLE_LOGIN_VIEW);
        presenter = new LoginPresenter(this, this);
        noDataLayout = findViewById(R.id.noDataLayout);
        progressBarLayout = findViewById(R.id.progressBarLayout);
        checkFlagExit();
        Session.getInstance().initializeGoogleClient(this);
        signInButton = findViewById(R.id.signInButton);
        signInButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        signIn();
                    }
                }
        );
    }

    @Override
    protected void onResume() {
        super.onResume();
        Session.getInstance().silentSign(this, Session.DEFAULT_CODE, this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Session.DEFAULT_CODE) {
            hideLoadingScreen();
            Session.getInstance().signIn(this, data);
        }
    }

    public void updateUI(View v) {
        updateUILogin();
    }

    private void setTitle(String title) {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(title);
        }
    }

    @Override
    public void updateUILogin() {
        if (Session.getInstance().getGoogleAccount() != null) {
            Log.d(LOG_TAG, "LoginView: Inicio HomeView Activity");
            Intent i = new Intent(this, HomeView.class);
            startActivity(i);
        } else {
            Log.d(LOG_TAG, "LoginView: Me quedo en LoginView");
        }
    }

    private void signIn() {
        showLoadingScreen();
        signInButton.setSize(SignInButton.SIZE_STANDARD);
        Intent signInIntent = Session.getInstance().getGoogleSignInClient().getSignInIntent();
        startActivityForResult(signInIntent, Session.DEFAULT_CODE);
    }

    private void checkFlagExit() {
        Intent intent = getIntent();
        boolean closeApp = intent.getBooleanExtra(StaticStrings.FLAG_EXIT, false);
        if (closeApp) {
            finishAffinity();
        }
    }

    public void exit(View v) {
        this.finishAffinity();
    }

    @Override
    public void showLoadingScreen() {
        Log.d(LOG_TAG, "LoginView: showLoadingScreen");
        progressBarLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoadingScreen() {
        Log.d(LOG_TAG, "LoginView: hideLoadingScreen");
        progressBarLayout.setVisibility(View.GONE);
    }

    @Override
    public void showErrorScreen() {
        Log.d(LOG_TAG, "LoginView: showErrorScreen)");
        noDataLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideErrorScreen() {
        Log.d(LOG_TAG, "LoginView: hideErrorScreen)");
        noDataLayout.setVisibility(View.GONE);
    }

    @Override
    public void onFinishedLogin(int resultCode) {
        presenter.authorizeLogin();
        presenter.sendFirebaseToken();
        Log.d(LOG_TAG, "LoginView (onFinishedLogin)");
    }

    @Override
    public void onFailureLogin() {
        Log.d(LOG_TAG, "LoginView (onFailureLogin)");
        hideLoadingScreen();
    }

}
