package com.uruguay.obligatoriofrontend.ui.showpublications;


import android.util.Log;

import com.uruguay.obligatoriofrontend.HttpCodes;
import com.uruguay.obligatoriofrontend.StaticStrings;
import com.uruguay.obligatoriofrontend.dto.PublicationDTO;
import com.uruguay.obligatoriofrontend.retrofit.IRetrofitApiConfigMVP;
import com.uruguay.obligatoriofrontend.retrofit.RetrofitAppConfig;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.uruguay.obligatoriofrontend.StaticStrings.LOG_TAG;

public class ShowPublicationsInterceptor implements IShowPublicationsMVP.Interactor {

    private IShowPublicationsMVP.Presenter presenter;

    public ShowPublicationsInterceptor(IShowPublicationsMVP.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void getMyPublications(final IShowPublicationsMVP.Interactor.OnFinishedListener onFinishedListener) {
        IRetrofitApiConfigMVP getResponse = RetrofitAppConfig.getRetrofit(presenter.getContext()).create(IRetrofitApiConfigMVP.class);
        Call<List<PublicationDTO>> call = getResponse.getMyPublications();
        call.enqueue(new Callback<List<PublicationDTO>>() {

            @Override
            public void onResponse(Call<List<PublicationDTO>> call, Response<List<PublicationDTO>> response) {
                if (response.code() == HttpCodes.OK) {
                    Log.d(LOG_TAG, "ShowPublicationInteractor: Ejecucion correcta");
                    List<PublicationDTO> publications = response.body();
                    Log.d(LOG_TAG, "ShowPublicationInteractor: getMyPublications " + publications.size() + "");
                    onFinishedListener.onFinished(publications);
                } else if(response.code() == HttpCodes.UNAUTHORIZED){
                    presenter.showError(StaticStrings.UNAUTHORIZED);
                }else {
                    Log.d(LOG_TAG, "ShowPublicationInteractor: else");
                    onFinishedListener.onFailure();
                }
            }

            @Override
            public void onFailure(Call<List<PublicationDTO>> call, Throwable t) {
                Log.d(LOG_TAG, "ShowPublicationInteractor: Error en retrofit: " + t.toString());
                onFinishedListener.onFailure();
            }
        });

    }

    @Override
    public void getMyPublicationsAnnotated(final OnFinishedListener onFinishedListener) {
        IRetrofitApiConfigMVP getResponse = RetrofitAppConfig.getRetrofit(presenter.getContext()).create(IRetrofitApiConfigMVP.class);
        Call<List<PublicationDTO>> call = getResponse.getMyPublicationsAnnotated();
        call.enqueue(new Callback<List<PublicationDTO>>() {

            @Override
            public void onResponse(Call<List<PublicationDTO>> call, Response<List<PublicationDTO>> response) {
                if (response.code() == HttpCodes.OK) {
                    Log.d(LOG_TAG, "ShowPublicationsInterceptor: Retrofit correcto");
                    List<PublicationDTO> publications = response.body();
                    Log.d(LOG_TAG, "ShowPublicationsInterceptor: getMyPublicationsAnnotated " + publications.size() + "");
                    onFinishedListener.onFinished(publications);
                } else if(response.code() == HttpCodes.UNAUTHORIZED){
                    presenter.showError(StaticStrings.UNAUTHORIZED);
                }else {
                    Log.d(LOG_TAG, "ShowPublicationsInterceptor: else");
                    onFinishedListener.onFailure();
                }
            }

            @Override
            public void onFailure(Call<List<PublicationDTO>> call, Throwable t) {
                Log.d(LOG_TAG, "ShowPublicationsInterceptor: Error en retrofit: " + t.toString());
                onFinishedListener.onFailure();
            }
        });
    }


}
