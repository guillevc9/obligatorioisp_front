package com.uruguay.obligatoriofrontend.ui.newanswer;

import android.util.Log;

import com.uruguay.obligatoriofrontend.HttpCodes;
import com.uruguay.obligatoriofrontend.StaticStrings;
import com.uruguay.obligatoriofrontend.dto.QuestionDTO;
import com.uruguay.obligatoriofrontend.retrofit.IRetrofitApiConfigMVP;
import com.uruguay.obligatoriofrontend.retrofit.RetrofitAppConfig;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.uruguay.obligatoriofrontend.StaticStrings.LOG_TAG;

public class NewAnswerInteractor implements INewAnswerMVP.Interactor {

    private INewAnswerMVP.Presenter presenter;

    public NewAnswerInteractor(INewAnswerMVP.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void newAnswer(int idPublication, QuestionDTO answer, final INewAnswerMVP.Interactor.OnFinishedListener onFinishedListener) {
        IRetrofitApiConfigMVP getResponse = RetrofitAppConfig.getRetrofit(presenter.getContext()).create(IRetrofitApiConfigMVP.class);
        String url = String.format("publication/%s/question/answer", idPublication);
        Log.d(LOG_TAG, "NewAnswerInteractor: newAnswer ID: " + idPublication + " URL: " + url);
        Log.d(LOG_TAG, "NewAnswerInteractor: newAnswer answer: " + answer);
        Call<Void> call = getResponse.newAnswer(answer, url);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Log.d(LOG_TAG, "NewAnswerInteractor: newAnswer response.code(): " + response.code());
                if (response.code() == HttpCodes.OK) {
                    Log.d(LOG_TAG, "NewAnswerInteractor: onResponse OK");
                    onFinishedListener.onFinished();
                } else if(response.code() == HttpCodes.UNAUTHORIZED){
                    presenter.showError(StaticStrings.UNAUTHORIZED);
                }else {
                    Log.d(LOG_TAG, "NewAnswerInteractor: onResponse ELSE");
                    onFinishedListener.onFailure();
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.d(LOG_TAG, "NewAnswerInteractor: onFailure");
                onFinishedListener.onFailure();
            }
        });
    }

}
