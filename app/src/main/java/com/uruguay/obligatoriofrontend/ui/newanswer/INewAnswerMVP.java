package com.uruguay.obligatoriofrontend.ui.newanswer;

import android.content.Context;

import com.uruguay.obligatoriofrontend.dto.QuestionDTO;

public interface INewAnswerMVP {

    interface View {

        public void showError(String error);

        public void showMessage(String messaje);

        public void finishActivity();

        void showLoadingScreen();

        void hideLoadingScreen();

        void showErrorScreen();

        void hideErrorScreen();

    }

    interface Presenter {

        public void showError(String error);

        public Context getContext();

        public void newAnswer(int idPublication, QuestionDTO answer);

    }

    interface Interactor {

        interface OnFinishedListener {
            void onFinished();

            void onFailure();
        }

        public void newAnswer(int idPublication, QuestionDTO answer, INewAnswerMVP.Interactor.OnFinishedListener onFinishedListener);

    }


}
