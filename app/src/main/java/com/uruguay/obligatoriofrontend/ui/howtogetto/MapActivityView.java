package com.uruguay.obligatoriofrontend.ui.howtogetto;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.uruguay.obligatoriofrontend.BuildConfig;
import com.uruguay.obligatoriofrontend.StaticMenu;
import com.uruguay.obligatoriofrontend.R;
import com.uruguay.obligatoriofrontend.sesion.Session;
import com.uruguay.obligatoriofrontend.ui.about.AboutView;
import com.uruguay.obligatoriofrontend.ui.home.HomeView;
import com.uruguay.obligatoriofrontend.ui.login.LoginView;
import com.uruguay.obligatoriofrontend.ui.showpublications.ShowPublicationsView;

import java.util.List;

import static com.uruguay.obligatoriofrontend.StaticStrings.ADDRESS;
import static com.uruguay.obligatoriofrontend.StaticStrings.FLAG_MYPUBLICATIONS_OR_ANNOTATED;
import static com.uruguay.obligatoriofrontend.StaticStrings.FLAG_MY_PUBLICATIONS;
import static com.uruguay.obligatoriofrontend.StaticStrings.FLAG_MY_PUBLICATIONS_ANNOTATED;
import static com.uruguay.obligatoriofrontend.StaticStrings.LOG_TAG;
import static com.uruguay.obligatoriofrontend.StaticStrings.TITLE_MAP_VIEW;

public class MapActivityView extends AppCompatActivity implements OnMapReadyCallback, TaskLoadedCallback, LocationListener {

    private GoogleMap mMap;
    private MarkerOptions placeByGps;
    private MarkerOptions placePublication;
    private Button btnMap;
    private Polyline currentPolyline;
    private static final int REQUEST_GPS_PERMISSION = 42;
    private LocationManager locationManager;
    private MapFragment mapFragment;
    private LatLng latLngPublication;
    private String address;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_map);
        btnMap = findViewById(R.id.btnMap);
        MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.mapFragment);
        address = getIntent().getStringExtra(ADDRESS);
        Log.d(LOG_TAG, "MapActivity : onCreate: address: " + address);

        latLngPublication = getLocationFromAddress(address);
        if (latLngPublication == null) {
            Log.d(LOG_TAG, "MapActivity : onCreate: latLngPublication: null");
            finish();
        } else {
            Log.d(LOG_TAG, "MapActivity : onCreate: latLngPublication: lon: " + latLngPublication.longitude + " lat: " + latLngPublication.latitude);
            placePublication = new MarkerOptions().position(latLngPublication).title("Destino");
            mapFragment.getMapAsync(this);
            setTitle(TITLE_MAP_VIEW);
            getLocation();
        }

    }

    public void showAddress(View view) {
        LocationManager lm = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;

        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
        }

        if (placeByGps == null && !gps_enabled) {
            Toast.makeText(this, "Debe encender el GPS para obtener la ruta", Toast.LENGTH_LONG).show();
        } else if (placeByGps == null && gps_enabled) {
            Toast.makeText(this, "Obteniendo datos del GPS, por favor espere", Toast.LENGTH_LONG).show();
            getLocation();
        }

        if (placeByGps != null) {
            new FetchURL(MapActivityView.this).execute(getUrl(placeByGps.getPosition(), placePublication.getPosition(), "driving"), "driving");
        }
    }

    public LatLng getLocationFromAddress(String strAddress) {
        Geocoder coder = new Geocoder(this);
        List<Address> address;
        LatLng p1 = null;

        try {
            address = coder.getFromLocationName(strAddress, 5);

            Address location = address.get(0);
            location.getLatitude();
            location.getLongitude();

            Log.d(LOG_TAG, "MapActivity : getLocationFromAddress: lon: " + location.getLongitude() + " lat: " + location.getLatitude());
            p1 = new LatLng(location.getLatitude(),
                    location.getLongitude());


        } catch (Exception e) {
            Toast.makeText(this, "Direccion incorrecta", Toast.LENGTH_LONG).show();
            return null;
        }
        return p1;
    }

    @Override
    public void onPause() {
        Log.d(LOG_TAG, "MapActivity: onPause: removeUpdates");
        super.onPause();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        Log.d(LOG_TAG, "Added Markers");
        mMap.addMarker(placePublication);

        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(placePublication.getPosition(), 15));
    }

    private String getUrl(LatLng origin, LatLng dest, String directionMode) {
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;
        String mode = "mode=" + directionMode;
        String parameters = str_origin + "&" + str_dest + "&" + mode;
        String output = "json";
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters + "&key=" + BuildConfig.MapsApiKey;
        Log.d(LOG_TAG, "MapActivity: getUrl: " + url);
        return url;
    }

    @Override
    public void onTaskDone(Object... values) {
        if (currentPolyline != null)
            currentPolyline.remove();
        currentPolyline = mMap.addPolyline((PolylineOptions) values[0]);
    }

    public void getLocation() {
        Log.d(LOG_TAG, "MapActivity: getLocation");
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Log.d(LOG_TAG, "MapActivity: getLocation: Permission not granted");
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_GPS_PERMISSION);
        } else {
            Log.d(LOG_TAG, "MapActivity: getLocation: Permission granted");
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_GPS_PERMISSION: {
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    Log.d(LOG_TAG, "MapActivity: getLocation: Permission not granted");
                } else {
                    Log.d(LOG_TAG, "MapActivity: getLocation: Permission granted");
                    locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
                }
            }
            return;
        }
    }

    @Override
    public void onProviderDisabled(String provider) {
        Log.d(LOG_TAG, "MapActivity: onProviderDisabled");
        Log.d("Latitude", "disable");
    }

    @Override
    public void onProviderEnabled(String provider) {
        Log.d(LOG_TAG, "MapActivity: onProviderEnabled");
        Log.d("Latitude", "enable");
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Log.d(LOG_TAG, "MapActivity: getLocation: Permission not granted");
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_GPS_PERMISSION);
        } else {
            Log.d(LOG_TAG, "MapActivity: getLocation: Permission granted");
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
        }
    }

    @Override
    public void onLocationChanged(Location location) {

            Log.d(LOG_TAG, "MapActivity: onLocationChanged: Latitude" + location.getLatitude() + ", Longitude: " + location.getLongitude());
        if(mMap!=null) {
            placeByGps = new MarkerOptions().position(new LatLng(location.getLatitude(), location.getLongitude())).title("Origen")
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));

                Marker origin = mMap.addMarker(placeByGps);
                origin.showInfoWindow();
                LatLngBounds.Builder builder = new LatLngBounds.Builder();
                builder.include(placeByGps.getPosition());
                builder.include(placePublication.getPosition());
                LatLngBounds bounds = builder.build();
                int padding = 80; //pixels
                mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, padding));
                showAddress(null);
            }else {
            locationManager.removeUpdates(this);
            Log.d(LOG_TAG, "MapActivity: onLocationChanged: mMap NULL");
        }

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        Log.d(LOG_TAG, "MapActivity: onStatusChanged: " + status);
        Log.d("Latitude", "status");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case R.id.item_home:
                intent = new Intent(this, HomeView.class);
                startActivity(intent);
                break;
            case R.id.item_my_publications:
                intent = new Intent(this, ShowPublicationsView.class);
                intent.putExtra(FLAG_MYPUBLICATIONS_OR_ANNOTATED, FLAG_MY_PUBLICATIONS);
                startActivity(intent);

                break;
            case R.id.item_publications_annotated:
                intent = new Intent(this, ShowPublicationsView.class);
                intent.putExtra(FLAG_MYPUBLICATIONS_OR_ANNOTATED, FLAG_MY_PUBLICATIONS_ANNOTATED);
                startActivity(intent);

                break;
            case R.id.item_about:
                startActivity(new Intent(this, AboutView.class));
                break;
            case R.id.item_logout:
                Session.getInstance().getGoogleSignInClient().signOut()
                        .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                Intent i = new Intent(MapActivityView.this, LoginView.class);
                                startActivity(i);
                            }
                        });
                break;
            case R.id.item_exit:
                StaticMenu.exit(this);
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    private void setTitle(String title) {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(title);
        }
    }

}
