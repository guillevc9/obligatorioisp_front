package com.uruguay.obligatoriofrontend.ui.publication;

import android.content.Context;

import com.uruguay.obligatoriofrontend.dto.PublicationDTO;

import java.util.List;

public class PublicationPresenter implements IPublicationMVP.Presenter {

    private IPublicationMVP.View view;
    private IPublicationMVP.Interactor interactor;
    private Context context;

    public PublicationPresenter(IPublicationMVP.View view, Context context) {
        this.view = view;
        this.context = context;
        this.interactor = new PublicationInteractor(this);
    }

    @Override
    public Context getContext() {
        return context;
    }

    @Override
    public void emptyEditTexts() {
        view.emptyFields();
    }

    @Override
    public void showError(String error) {
        view.showError(error);
    }

    @Override
    public void showMessage(String messaje) {
        view.showMessage(messaje);
    }

    @Override
    public void sendUpdatePublication(PublicationDTO publication, List<String> imagePaths) {
        interactor.sendUpdatePublication(publication, imagePaths);
    }

    @Override
    public void sendNewPublication(PublicationDTO publication, List<String> imagePaths) {
        interactor.sendNewPublication(publication, imagePaths);
    }

    @Override
    public void finishActivity() {
        view.finishActivity();
    }


}
