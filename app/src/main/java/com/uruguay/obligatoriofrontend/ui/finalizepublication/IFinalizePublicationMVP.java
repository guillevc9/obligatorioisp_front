package com.uruguay.obligatoriofrontend.ui.finalizepublication;

import android.content.Context;

import com.uruguay.obligatoriofrontend.dto.FinalizePublicationDTO;
import com.uruguay.obligatoriofrontend.dto.UserDTO;

import java.util.List;

public interface IFinalizePublicationMVP {

    interface View {

        public void showError(String error);

        public void showMessage(String messaje);

        public void setMotivesToSpinner(List<String> motives);

        public void showUsersAnnotatedFields();

        public void setUsers(List<UserDTO> users);

        public void getDataFromServer(int idPublication);

        public void sendFinalize(FinalizePublicationDTO finalizePublicationDTO);

        public void finishActiviry();
    }

    interface Presenter {

        public void getDataFromServer(int idPublication);

        public Context getContext();

        public void sendFinalize(FinalizePublicationDTO finalizePublicationDTO);

    }

    interface Interactor {

        interface OnFinishedListener {
            void onFinished(List<UserDTO> users);

            void onFailure();
        }

        interface OnFinishedListenerFinalize {
            void onFinishedFinalize();

            void onFailureFinalize(String error);
        }

        public void sendFinalize(FinalizePublicationDTO finalizePublicationDTO, OnFinishedListenerFinalize onFinishedListenerFinalize);

        public void getUsersAnnotated(int idPublication, OnFinishedListener onFinishedListener);
    }

}
