package com.uruguay.obligatoriofrontend.ui.newanswer;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.uruguay.obligatoriofrontend.R;
import com.uruguay.obligatoriofrontend.dto.QuestionDTO;

import static com.uruguay.obligatoriofrontend.StaticStrings.ANSWER;
import static com.uruguay.obligatoriofrontend.StaticStrings.LOG_TAG;
import static com.uruguay.obligatoriofrontend.StaticStrings.NEW_ANSWER_EMPTY;
import static com.uruguay.obligatoriofrontend.StaticStrings.PUBLICATION_ID;
import static com.uruguay.obligatoriofrontend.StaticStrings.QUESTION;
import static com.uruguay.obligatoriofrontend.StaticStrings.QUESTION_ID;
import static com.uruguay.obligatoriofrontend.StaticStrings.TITLE_NEW_ANSWER_VIEW;

public class NewAnswerView extends AppCompatActivity implements INewAnswerMVP.View {

    private static final int MAX_LENGTH_ANSWER = 140;
    private TextView tvQuestion;
    private EditText etAnswer;
    private int idPublication;
    private INewAnswerMVP.Presenter presenter;
    private RelativeLayout noDataLayout;
    private RelativeLayout progressBarLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_new_answer);
        setTitle(TITLE_NEW_ANSWER_VIEW);
        initialize();
    }

    private void initialize() {
        tvQuestion = findViewById(R.id.tvQuestion);
        etAnswer = findViewById(R.id.etAnswer);
        idPublication = getIntent().getIntExtra(PUBLICATION_ID, 0);
        tvQuestion.setText(getIntent().getStringExtra(QUESTION));
        etAnswer.setText(getIntent().getStringExtra(ANSWER));
        presenter = new NewAnswerPresenter(this, this);
        noDataLayout = findViewById(R.id.noDataLayout);
        progressBarLayout = findViewById(R.id.progressBarLayout);
    }

    public void onSendNewAnswer(View view) {
        Log.d(LOG_TAG, "NewAnswerView: onSendNewAnswer");
        String answer = etAnswer.getText().toString();
        if (answer.isEmpty()) {
            showError(NEW_ANSWER_EMPTY);
        }  else if (answer.length() > MAX_LENGTH_ANSWER) {
            String error = "La respuesta no puede tener más de " + MAX_LENGTH_ANSWER + " caracteres";
            showError(error);
        }else {
            showLoadingScreen();
            QuestionDTO questionDTO = new QuestionDTO();
            questionDTO.setQuestionId(getIntent().getIntExtra(QUESTION_ID,0));
            questionDTO.setQuestion(getIntent().getStringExtra(QUESTION));
            questionDTO.setAnswer(etAnswer.getText().toString());
            presenter.newAnswer(idPublication, questionDTO);
        }
    }

    public void onCancelNewAnswer(View view) {
        finish();
    }

    public void finishActivity() {
        finish();
    }

    @Override
    public void showError(String error) {
        Toast.makeText(NewAnswerView.this, error, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(NewAnswerView.this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showLoadingScreen() {
        Log.d(LOG_TAG, "ShowPublicationDetailsView: showLoadingScreen");
        progressBarLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoadingScreen() {
        Log.d(LOG_TAG, "ShowPublicationDetailsView: hideLoadingScreen");
        progressBarLayout.setVisibility(View.GONE);
    }

    @Override
    public void showErrorScreen() {
        Log.d(LOG_TAG, "ShowPublicationDetailsView: showErrorScreen)");
        noDataLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideErrorScreen() {
        Log.d(LOG_TAG, "ShowPublicationDetailsView: hideErrorScreen)");
        noDataLayout.setVisibility(View.GONE);
    }
}
