package com.uruguay.obligatoriofrontend.ui.publication;

import android.content.Context;

import com.uruguay.obligatoriofrontend.dto.PublicationDTO;

import java.util.List;

public interface IPublicationMVP {

    interface View {
        public void showError(String error);

        public void showMessage(String messaje);

        public void sendNewPublication(PublicationDTO publication);

        public void sendUpdatePublication(PublicationDTO publication);

        public void finishActivity();

        public void emptyFields();

        void showLoadingScreen();

        void hideLoadingScreen();

        void showErrorScreen();

        void hideErrorScreen();
    }

    interface Presenter {
        public Context getContext();

        public void emptyEditTexts();

        public void showError(String error);

        public void showMessage(String messaje);

        public void sendUpdatePublication(PublicationDTO publication, List<String> imagePaths);

        public void sendNewPublication(PublicationDTO publication, List<String> imagePaths);

        public void finishActivity();
    }

    interface Interactor {
        public void sendNewPublication(PublicationDTO publication, List<String> imagePaths);

        public void sendUpdatePublication(PublicationDTO publication, List<String> imagePaths);

    }

}
