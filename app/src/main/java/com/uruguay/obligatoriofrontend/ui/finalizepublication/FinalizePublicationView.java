package com.uruguay.obligatoriofrontend.ui.finalizepublication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.uruguay.obligatoriofrontend.R;

import com.uruguay.obligatoriofrontend.dto.FinalizePublicationDTO;
import com.uruguay.obligatoriofrontend.dto.UserDTO;

import java.util.ArrayList;
import java.util.List;

import static com.uruguay.obligatoriofrontend.StaticStrings.ID;
import static com.uruguay.obligatoriofrontend.StaticStrings.LOG_TAG;
import static com.uruguay.obligatoriofrontend.StaticStrings.MOTIVE_MANDATORY;
import static com.uruguay.obligatoriofrontend.StaticStrings.REASON_NO_USERS;
import static com.uruguay.obligatoriofrontend.StaticStrings.REASON_OTHER;
import static com.uruguay.obligatoriofrontend.StaticStrings.REASON_SELECT_CANDIDATE;
import static com.uruguay.obligatoriofrontend.StaticStrings.TITLE;
import static com.uruguay.obligatoriofrontend.StaticStrings.TITLE_FINALIZE_PUBLICATION_VIEW;

public class FinalizePublicationView extends AppCompatActivity implements IFinalizePublicationMVP.View {

    private TextView tvTitleFP;
    private Spinner spinnerMotivesFP;
    private EditText edOtherMotiveFP;
    private Spinner spinnerUsersFP;
    private IFinalizePublicationMVP.Presenter presenter;
    private List<UserDTO> users;
    private int idPublication;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_finalize_publication);
        setTitle(TITLE_FINALIZE_PUBLICATION_VIEW);
        initialize();
        setListenerToSpinnerMotives();
        getData();
    }

    private void initialize() {
        tvTitleFP = findViewById(R.id.tvTitleFP);
        spinnerMotivesFP = findViewById(R.id.spinnerMotivesFP);
        edOtherMotiveFP = findViewById(R.id.edOtherMotiveFP);
        spinnerUsersFP = findViewById(R.id.spinnerUsersFP);
        presenter = new FinalizePublicationPresenter(this, this);
        users = new ArrayList<>();
        idPublication = getIntent().getIntExtra(ID, 0);
    }

    private void getData() {
        Intent intent = getIntent();
        tvTitleFP.setText(intent.getStringExtra(TITLE));
        getDataFromServer(idPublication);
    }

    private void setListenerToSpinnerMotives() {
        spinnerMotivesFP.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                Log.d(LOG_TAG, "FinalizePublication: update desde  setListenerToSpinnerMotives");
                updateUI();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                Log.d(LOG_TAG, "FinalizePublication: update  onNothingSelected");
            }
        });
    }

    private void updateUI() {
        String selec = spinnerMotivesFP.getSelectedItem().toString();
        if (selec.equals(REASON_SELECT_CANDIDATE)) {
            spinnerUsersFP.setVisibility(Spinner.VISIBLE);
            edOtherMotiveFP.setVisibility(EditText.INVISIBLE);
        } else if (selec.equals(REASON_NO_USERS)) {
            spinnerUsersFP.setVisibility(Spinner.INVISIBLE);
            edOtherMotiveFP.setVisibility(EditText.INVISIBLE);
        } else if (selec.equals(REASON_OTHER)) {
            spinnerUsersFP.setVisibility(Spinner.INVISIBLE);
            edOtherMotiveFP.setVisibility(EditText.VISIBLE);
        }
    }

    public void finalize(View view) {
        boolean valid = true;
        String reason = spinnerMotivesFP.getSelectedItem().toString();
        String idGoogleUser = "";
        String otherReason = "";
        if (reason.equals(REASON_SELECT_CANDIDATE)) {
            int pos = spinnerUsersFP.getSelectedItemPosition();
            Log.d(LOG_TAG, "FinalizePublication: POSSSSSSSSS " + pos);
            Log.d(LOG_TAG, "FinalizePublication: SIZEeeee " + users.size());
            idGoogleUser = users.get(pos).getIdGoogle();
        } else if (reason.equals(REASON_OTHER)) {
            otherReason = edOtherMotiveFP.getText().toString();
            if (otherReason.trim().equals("")) {
                showError(MOTIVE_MANDATORY);
                valid = false;
            }
        }
        if (valid) {
            FinalizePublicationDTO finalizePublicationDTO = new FinalizePublicationDTO(idPublication, reason, idGoogleUser, otherReason);
            sendFinalize(finalizePublicationDTO);
        }
    }

    @Override
    public void showError(String error) {
        Toast.makeText(FinalizePublicationView.this, error, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showMessage(String messaje) {
        Toast.makeText(FinalizePublicationView.this, messaje, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void setMotivesToSpinner(List<String> motives) {
        spinnerMotivesFP.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, motives));
    }

    @Override
    public void showUsersAnnotatedFields() {
        spinnerUsersFP.setVisibility(Spinner.VISIBLE);
    }

    @Override
    public void getDataFromServer(int idPublication) {
        presenter.getDataFromServer(idPublication);
    }

    @Override
    public void sendFinalize(FinalizePublicationDTO finalizePublicationDTO) {
        presenter.sendFinalize(finalizePublicationDTO);
    }

    @Override
    public void finishActiviry() {
        finish();
    }

    @Override
    public void setUsers(List<UserDTO> users) {
        this.users = users;
        spinnerUsersFP.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, users));
    }

}
