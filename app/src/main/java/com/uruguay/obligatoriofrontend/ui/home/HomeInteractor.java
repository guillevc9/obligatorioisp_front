package com.uruguay.obligatoriofrontend.ui.home;

import android.util.Log;

import com.uruguay.obligatoriofrontend.HttpCodes;
import com.uruguay.obligatoriofrontend.StaticStrings;
import com.uruguay.obligatoriofrontend.dto.CategoryDTO;
import com.uruguay.obligatoriofrontend.dto.PublicationDTO;
import com.uruguay.obligatoriofrontend.dto.ZoneDTO;
import com.uruguay.obligatoriofrontend.retrofit.IRetrofitApiConfigMVP;
import com.uruguay.obligatoriofrontend.retrofit.RetrofitAppConfig;
import com.uruguay.obligatoriofrontend.sesion.Session;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.uruguay.obligatoriofrontend.StaticStrings.LOG_TAG;

public class HomeInteractor implements IHomeMVP.Interactor {

    List<PublicationDTO> publications = new ArrayList<>();

    private IHomeMVP.Presenter presenter;

    public HomeInteractor(IHomeMVP.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void getPublications(String zone, String category, final OnFinishedListener onFinishedListener) {
        IRetrofitApiConfigMVP getResponse = RetrofitAppConfig.getRetrofit(presenter.getContext()).create(IRetrofitApiConfigMVP.class);
        Call<List<PublicationDTO>> call = getResponse.getPublications(category, zone);
        call.enqueue(new Callback<List<PublicationDTO>>() {

            @Override
            public void onResponse(Call<List<PublicationDTO>> call, Response<List<PublicationDTO>> response) {
                if (response.code() == HttpCodes.OK) {
                    Log.d(LOG_TAG, "HomeInteractor: getPublications correcto");
                    List<PublicationDTO> publications = response.body();
                    Log.d(LOG_TAG, "HomeInteractor: getPublications cantidad: " + publications.size());
                    onFinishedListener.onFinished(publications);
                } else if(response.code() == HttpCodes.UNAUTHORIZED){
                    presenter.showError(StaticStrings.UNAUTHORIZED);
                }else {
                    Log.d(LOG_TAG, "HomeInteractor: getPublications else");
                    onFinishedListener.onFailure();
                }
            }

            @Override
            public void onFailure(Call<List<PublicationDTO>> call, Throwable t) {
                Log.d(LOG_TAG, "HomeInteractor: getPublications error en Retrofit" + t.toString());
                onFinishedListener.onFailure();
            }
        });
    }

    @Override
    public void getZones() {
        IRetrofitApiConfigMVP getResponse = RetrofitAppConfig.getRetrofit(presenter.getContext()).create(IRetrofitApiConfigMVP.class);
        Call<List<ZoneDTO>> call = getResponse.getZones();
        call.enqueue(new Callback<List<ZoneDTO>>() {

            @Override
            public void onResponse(Call<List<ZoneDTO>> call, Response<List<ZoneDTO>> response) {
                if (response.code() == HttpCodes.OK) {
                    List<ZoneDTO> zones = response.body();
                    Session.getInstance().setZones(zones);
                    presenter.setZonesToSpinner();
                } else if(response.code() == HttpCodes.UNAUTHORIZED){
                    presenter.showError(StaticStrings.UNAUTHORIZED);
                }else {
                    Session.getInstance().setZones(getZonesNoConection());
                    presenter.setZonesToSpinner();
                }
            }

            @Override
            public void onFailure(Call<List<ZoneDTO>> call, Throwable t) {

                Session.getInstance().setZones(getZonesNoConection());
                presenter.setZonesToSpinner();
            }
        });
    }

    @Override
    public void getCategories() {
        IRetrofitApiConfigMVP getResponse = RetrofitAppConfig.getRetrofit(presenter.getContext()).create(IRetrofitApiConfigMVP.class);
        Call<List<CategoryDTO>> call = getResponse.getCategories();
        call.enqueue(new Callback<List<CategoryDTO>>() {

            @Override
            public void onResponse(Call<List<CategoryDTO>> call, Response<List<CategoryDTO>> response) {
                if (response.code() == HttpCodes.OK) {
                    List<CategoryDTO> categories = response.body();
                    Session.getInstance().setCategories(categories);
                    presenter.setCategoriesToSpinner();
                } else if(response.code() == HttpCodes.UNAUTHORIZED){
                    presenter.showError(StaticStrings.UNAUTHORIZED);
                }else {
                    Session.getInstance().setCategories(getCategoriesNoConection());
                    presenter.setCategoriesToSpinner();
                }
            }

            @Override
            public void onFailure(Call<List<CategoryDTO>> call, Throwable t) {
                Session.getInstance().setCategories(getCategoriesNoConection());
                presenter.setCategoriesToSpinner();
            }
        });
    }

    private List<CategoryDTO> getCategoriesNoConection() {
        List<CategoryDTO> categories = new ArrayList<>();
        categories.add(new CategoryDTO("Construcción"));
        categories.add(new CategoryDTO("Jardinería"));
        return categories;
    }

    private List<ZoneDTO> getZonesNoConection() {
        List<ZoneDTO> zones = new ArrayList<>();
        zones.add(new ZoneDTO("Lezica"));
        zones.add(new ZoneDTO("Melilla"));
        zones.add(new ZoneDTO("Paso de la arena"));
        zones.add(new ZoneDTO("Otra"));
        return zones;
    }
}
