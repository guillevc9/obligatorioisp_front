package com.uruguay.obligatoriofrontend.ui.newquestion;

import android.content.Context;

import com.uruguay.obligatoriofrontend.dto.QuestionDTO;

import static com.uruguay.obligatoriofrontend.StaticStrings.NEW_QUESTION_ERROR;
import static com.uruguay.obligatoriofrontend.StaticStrings.NEW_QUESTION_OK;

public class NewQuestionPresenter implements INewQuestionMVP.Presenter, INewQuestionMVP.Interactor.OnFinishedListener {

    private INewQuestionMVP.View view;
    private INewQuestionMVP.Interactor interactor;
    private Context context;

    public NewQuestionPresenter(INewQuestionMVP.View view, Context context) {
        this.view = view;
        this.context = context;
        this.interactor = new NewQuestionInteractor(this);
    }

    @Override
    public void showError(String error) {
        view.showError(error);
    }

    @Override
    public Context getContext() {
        return context;
    }

    @Override
    public void newQuestion(int idPublication, QuestionDTO question) {
        interactor.newQuestion(idPublication, question, this);
    }

    @Override
    public void onFinished() {
        view.hideLoadingScreen();
        view.showMessage(NEW_QUESTION_OK);
        view.finishActivity();
    }

    @Override
    public void onFailure() {
        view.hideLoadingScreen();
        view.showError(NEW_QUESTION_ERROR);
    }
}
