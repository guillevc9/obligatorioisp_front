package com.uruguay.obligatoriofrontend.ui.newanswer;

import android.content.Context;

import com.uruguay.obligatoriofrontend.dto.QuestionDTO;

import static com.uruguay.obligatoriofrontend.StaticStrings.NEW_ANSWER_ERROR;
import static com.uruguay.obligatoriofrontend.StaticStrings.NEW_ANSWER_OK;

public class NewAnswerPresenter implements INewAnswerMVP.Presenter, INewAnswerMVP.Interactor.OnFinishedListener {

    private INewAnswerMVP.View view;
    private INewAnswerMVP.Interactor interactor;
    private Context context;

    public NewAnswerPresenter(INewAnswerMVP.View view, Context context) {
        this.view = view;
        this.context = context;
        this.interactor = new NewAnswerInteractor(this);
    }

    @Override
    public void showError(String error) {
        view.showError(error);
    }

    @Override
    public Context getContext() {
        return context;
    }

    @Override
    public void newAnswer(int idPublication, QuestionDTO answer) {
        interactor.newAnswer(idPublication, answer, this);
    }

    @Override
    public void onFinished() {
        view.hideLoadingScreen();
        view.showMessage(NEW_ANSWER_OK);
        view.finishActivity();
    }

    @Override
    public void onFailure() {
        view.hideLoadingScreen();
        view.showError(NEW_ANSWER_ERROR);
    }

}
