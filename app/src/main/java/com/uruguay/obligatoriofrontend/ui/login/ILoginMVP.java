package com.uruguay.obligatoriofrontend.ui.login;

import android.content.Context;

public interface ILoginMVP {

    interface View {

        void showLoadingScreen();

        void hideLoadingScreen();

        void showErrorScreen();

        void hideErrorScreen();

        void updateUILogin();
    }

    interface Presenter {

        Context getContext();

        void sendFirebaseToken();

        void authorizeLogin();
    }

    interface Interactor {

        interface OnFinishedListener {
            void onFinished();

            void onFailure();
        }

        void authorizeLogin(OnFinishedListener onFinishedListener);

        void sendFirebaseToken();
    }
}
