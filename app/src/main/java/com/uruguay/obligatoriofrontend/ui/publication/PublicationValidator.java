package com.uruguay.obligatoriofrontend.ui.publication;

import com.uruguay.obligatoriofrontend.OperationResult;
import com.uruguay.obligatoriofrontend.dto.PublicationDTO;

public final class PublicationValidator {

    private static final int TITLE_MAX_LENGTH = 40;
    private static final int DESCRIPTION_MAX_LENGTH = 200;
    private static final String ERROR_EMPTY_TITLE = "El título no puede ser vacío.\n";
    private static final String ERROR_EMPTY_ADDRESS = "La dirección no puede ser vacía.\n";

    public static OperationResult validate(PublicationDTO publication) {
        OperationResult operationResult = new OperationResult();
        if (publication.getTitle().trim().equals("")) {
            operationResult.isValid = false;
            operationResult.message += ERROR_EMPTY_TITLE;
        }
        if (publication.getTitle().length() > TITLE_MAX_LENGTH) {
            operationResult.isValid = false;
            operationResult.message += "El título no puede tener más de " + TITLE_MAX_LENGTH + " caracteres.\n";
        }
        if (publication.getDescription().length() > DESCRIPTION_MAX_LENGTH) {
            operationResult.isValid = false;
            operationResult.message += "La descripción no puede tener más de " + DESCRIPTION_MAX_LENGTH + " caracteres.\n";
        }
        if (publication.getAddress().trim().equals("")) {
            operationResult.isValid = false;
            operationResult.message += ERROR_EMPTY_ADDRESS;
        }
        return operationResult;
    }

}
