package com.uruguay.obligatoriofrontend.ui.login;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.uruguay.obligatoriofrontend.HttpCodes;
import com.uruguay.obligatoriofrontend.notification.SendTokenToServer;
import com.uruguay.obligatoriofrontend.retrofit.IRetrofitApiConfigMVP;
import com.uruguay.obligatoriofrontend.retrofit.RetrofitAppConfig;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.uruguay.obligatoriofrontend.StaticStrings.LOG_TAG;

public class LoginInteractor implements ILoginMVP.Interactor {

    private ILoginMVP.Presenter presenter;

    public LoginInteractor(ILoginMVP.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void authorizeLogin(final OnFinishedListener onFinishedListener) {
        Log.d(LOG_TAG, "LoginInteractor: Corriendo Authorize Login");
        IRetrofitApiConfigMVP getResponse = RetrofitAppConfig.getRetrofit(presenter.getContext()).create(IRetrofitApiConfigMVP.class);
        Call<Void> call = getResponse.login();
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Log.d(LOG_TAG, "LoginInteractor: Response code Authorize Login: " + response.code());
                if (response.code() == HttpCodes.OK) {
                    onFinishedListener.onFinished();
                } else {
                    onFinishedListener.onFailure();
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.d(LOG_TAG, "LoginInteractor: ERROR en Authorize Login: " + t.getMessage());
                onFinishedListener.onFailure();
            }
        });
    }

    @Override
    public void sendFirebaseToken() {
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w(LOG_TAG, "getInstanceId failed", task.getException());
                            return;
                        }
                        // Get new Instance ID token
                        String token = task.getResult().getToken();
                        Log.d(LOG_TAG,"LoginViewImp: sendFirebaseToken: FCM: " + token);
                        SendTokenToServer.sendTokenToServer(presenter.getContext(), token);
                    }
                });
    }
}
