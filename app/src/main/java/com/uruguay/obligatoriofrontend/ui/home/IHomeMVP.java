package com.uruguay.obligatoriofrontend.ui.home;

import android.content.Context;

import com.uruguay.obligatoriofrontend.dto.PublicationDTO;

import java.util.ArrayList;
import java.util.List;

public interface IHomeMVP {

    interface View {

        public void setPublicationsToRecyclerView(List<PublicationDTO> publications);

        public void setZonesToSpinner();

        public void setCategoriesToSpinner();

        public void showError(String error);

        public void showMessage(String messaje);

        public void getPublications();

        public void getCategories();

        public void getZones();

        void showLoadingScreen();

        void hideLoadingScreen();

        void showErrorScreen();

        void hideErrorScreen();
    }

    interface Presenter {

        public void showError(String error);

        public Context getContext();

        public void setZonesToSpinner();

        public void setCategoriesToSpinner();

        public void getPublications(String zone, String category);

        public void getCategories();

        public void getZones();

    }

    interface Interactor {

        interface OnFinishedListener {
            void onFinished(List<PublicationDTO> publications);

            void onFailure();
        }

        public void getPublications(String zone, String category, OnFinishedListener onFinishedListener);

        public void getZones();

        public void getCategories();

    }

}
