package com.uruguay.obligatoriofrontend.ui.publication;

import android.util.Log;

import com.uruguay.obligatoriofrontend.HttpCodes;
import com.uruguay.obligatoriofrontend.StaticStrings;
import com.uruguay.obligatoriofrontend.dto.PublicationDTO;
import com.uruguay.obligatoriofrontend.retrofit.IRetrofitApiConfigMVP;
import com.uruguay.obligatoriofrontend.retrofit.RetrofitAppConfig;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.uruguay.obligatoriofrontend.StaticStrings.LOG_TAG;
import static com.uruguay.obligatoriofrontend.StaticStrings.UPLOAD_PHOTOS_ERROR;

public class PublicationInteractor implements IPublicationMVP.Interactor {

    private static final String MEDIA_TYPE_TEXT_PLAIN = "text/plain";
    private static final String MEDIA_TYPE_MULTIPART = "multipart/form-data";
    private static final String NAME_PHOTOS = "photos";

    private IPublicationMVP.Presenter presenter;

    public PublicationInteractor(IPublicationMVP.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void sendNewPublication(final PublicationDTO publication, final List<String> imagePaths) {
        Log.d("Obligatorio", "NewPublicationInteractor: Publicacion a enviar" + publication.toString());
        IRetrofitApiConfigMVP getResponse = RetrofitAppConfig.getRetrofit(presenter.getContext()).create(IRetrofitApiConfigMVP.class);
        Call<PublicationDTO> call = getResponse.newPublication(publication);
        call.enqueue(new Callback<PublicationDTO>() {
            @Override
            public void onResponse(Call<PublicationDTO> call, Response<PublicationDTO> response) {
                Log.d(LOG_TAG, "NewPublicationInteractor: response code: " + response.code());
                if (response.code() == HttpCodes.CREATED) {
                    PublicationDTO pub = response.body();
                    if (!imagePaths.isEmpty()) {
                        sendImages(pub.getId(), imagePaths, StaticStrings.ADD_PUBLICATION_OK);
                    } else {
                        presenter.showMessage(StaticStrings.ADD_PUBLICATION_OK);
                    }
                    presenter.finishActivity();
                } else if (response.code() == HttpCodes.UNAUTHORIZED) {
                    presenter.showError(StaticStrings.UNAUTHORIZED);
                } else {
                    presenter.showError(StaticStrings.ADD_PUBLICATION_ERROR);
                }
            }

            @Override
            public void onFailure(Call<PublicationDTO> call, Throwable t) {
                Log.d(LOG_TAG, "NewPublicationInteractor: error en retrofit: " + t.getMessage());
                presenter.showError(StaticStrings.ADD_PUBLICATION_ERROR);
            }
        });
    }

    public void sendImages(int idPublication, List<String> imagePaths, final String message) {
        Log.d(LOG_TAG, "ID: " + idPublication + " - " + imagePaths.size() + "");
        List<MultipartBody.Part> parts = new ArrayList<>();
        int i = 0;
        for (String item : imagePaths) {
            Log.d(LOG_TAG, "FOR: " + item);
            File file = new File(item);
            RequestBody requestBody1 = RequestBody.create(MediaType.parse(MEDIA_TYPE_MULTIPART), file);
            Log.d(LOG_TAG, "FOR: " + file.getName());
            MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData(NAME_PHOTOS, file.getName(), requestBody1);
            parts.add(fileToUpload);
        }
        Log.d(LOG_TAG, "PARTES: " + parts.size());
        RequestBody id_publication = RequestBody.create(
                MediaType.parse(MEDIA_TYPE_TEXT_PLAIN), String.valueOf(idPublication));

        IRetrofitApiConfigMVP getResponse = RetrofitAppConfig.getRetrofit(presenter.getContext()).create(IRetrofitApiConfigMVP.class);
        Call<Void> call = getResponse.uploadMultipleFilesDynamic(id_publication, parts);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                presenter.showMessage(message);
                Log.d(LOG_TAG, "Fotos agregadas correctamente.");
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                presenter.showError(UPLOAD_PHOTOS_ERROR);
                Log.d(LOG_TAG, "No se pudo subir las fotos.");
            }
        });
    }

    @Override
    public void sendUpdatePublication(PublicationDTO publication, final List<String> imagePaths) {
        Log.d("Obligatorio", publication.toString());
        IRetrofitApiConfigMVP getResponse = RetrofitAppConfig.getRetrofit(presenter.getContext()).create(IRetrofitApiConfigMVP.class);
        Call<PublicationDTO> call = getResponse.updatePublication(publication);
        call.enqueue(new Callback<PublicationDTO>() {
            @Override
            public void onResponse(Call<PublicationDTO> call, Response<PublicationDTO> response) {
                if (response.code() == HttpCodes.OK) {
                    PublicationDTO pub = response.body();
                    if (!imagePaths.isEmpty()) {
                        sendImages(pub.getId(), imagePaths, StaticStrings.UPDATE_PUBLICATIONS_OK);
                    } else {
                        presenter.showMessage(StaticStrings.UPDATE_PUBLICATIONS_OK);
                    }
                    presenter.finishActivity();
                } else if (response.code() == HttpCodes.UNAUTHORIZED) {
                    presenter.showError(StaticStrings.UNAUTHORIZED);
                } else {
                    presenter.showError(StaticStrings.UPDATE_PUBLICATIONS_ERROR);
                }
            }

            @Override
            public void onFailure(Call<PublicationDTO> call, Throwable t) {
                Log.d("Obligatorio", t.getMessage());
                presenter.showError(StaticStrings.UPDATE_PUBLICATIONS_ERROR);
            }
        });
    }


}
