package com.uruguay.obligatoriofrontend.ui.newquestion;

import android.content.Context;

import com.uruguay.obligatoriofrontend.dto.QuestionDTO;

public interface INewQuestionMVP {

    interface View {

        public void showError(String error);

        public void showMessage(String messaje);

        public void finishActivity();

        void showLoadingScreen();

        void hideLoadingScreen();

        void showErrorScreen();

        void hideErrorScreen();

    }

    interface Presenter {

        public void showError(String error);

        public Context getContext();

        public void newQuestion(int idPublication, QuestionDTO question);

    }

    interface Interactor {
        interface OnFinishedListener {
            void onFinished();

            void onFailure();
        }

        public void newQuestion(int idPublication, QuestionDTO question, OnFinishedListener onFinishedListener);

    }
}
