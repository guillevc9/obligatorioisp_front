package com.uruguay.obligatoriofrontend.ui.about;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.uruguay.obligatoriofrontend.R;

public class AboutView extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_about);
    }

}
