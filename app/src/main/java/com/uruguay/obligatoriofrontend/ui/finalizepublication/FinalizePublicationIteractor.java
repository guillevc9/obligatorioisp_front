package com.uruguay.obligatoriofrontend.ui.finalizepublication;

import android.util.Log;

import com.uruguay.obligatoriofrontend.HttpCodes;
import com.uruguay.obligatoriofrontend.dto.FinalizePublicationDTO;
import com.uruguay.obligatoriofrontend.dto.UserDTO;
import com.uruguay.obligatoriofrontend.retrofit.IRetrofitApiConfigMVP;
import com.uruguay.obligatoriofrontend.retrofit.RetrofitAppConfig;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.uruguay.obligatoriofrontend.StaticStrings.LOG_TAG;
import static com.uruguay.obligatoriofrontend.StaticStrings.UNAUTHORIZED;

public class FinalizePublicationIteractor implements IFinalizePublicationMVP.Interactor {

    private IFinalizePublicationMVP.Presenter presenter;

    public FinalizePublicationIteractor(IFinalizePublicationMVP.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void sendFinalize(final FinalizePublicationDTO finalizePublicationDTO, final OnFinishedListenerFinalize onFinishedListenerFinalize) {
        IRetrofitApiConfigMVP getResponse = RetrofitAppConfig.getRetrofit(presenter.getContext()).create(IRetrofitApiConfigMVP.class);
        Call<Void> call = getResponse.finalizePublication(finalizePublicationDTO);
        Log.d(LOG_TAG, "FinalizePublicationInteractor: sendFinalize " + finalizePublicationDTO);
        call.enqueue(new Callback<Void>() {

            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Log.d(LOG_TAG, "FinalizePublicationInteractor: onResponse response.code(): " + response.code());
                if(response.code() == HttpCodes.OK){
                    onFinishedListenerFinalize.onFinishedFinalize();
                }
                else if(response.code() == HttpCodes.UNAUTHORIZED){
                    onFinishedListenerFinalize.onFailureFinalize(UNAUTHORIZED);
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.d(LOG_TAG, "FinalizePublicationInteractor: onFailure ");
                onFinishedListenerFinalize.onFailureFinalize("Error al finalizar la publicación.");
            }
        });
    }

    @Override
    public void getUsersAnnotated(int idPublication, final OnFinishedListener onFinishedListener) {

        IRetrofitApiConfigMVP getResponse = RetrofitAppConfig.getRetrofit(presenter.getContext()).create(IRetrofitApiConfigMVP.class);
        String url = String.format("publication/%s/annotatedUsers/", idPublication);
        Call<List<UserDTO>> call = getResponse.getUsers(url);
        call.enqueue(new Callback<List<UserDTO>>() {

            @Override
            public void onResponse(Call<List<UserDTO>> call, Response<List<UserDTO>> response) {
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<List<UserDTO>> call, Throwable t) {
                onFinishedListener.onFailure();
            }
        });
    }


}
