package com.uruguay.obligatoriofrontend.ui.showpublicatondetails;

import android.content.Context;

import com.uruguay.obligatoriofrontend.dto.QuestionDTO;

import java.util.List;


public interface IShowPublicationDetails {


    interface View {

        public void setSwitchOn();

        public void setSwitchOff();

        public void showError(String error);

        public void showMessage(String messaje);

        void showLoadingScreen();

        void hideLoadingScreen();

        void showErrorScreen();

        void hideErrorScreen();

        public void setQuestionsToRecyclerView(List<QuestionDTO> questions);
    }

    interface Presenter {

        public void showError(String error);

        public void isAnnotated(int id);

        public void register(int id);

        public void rescind(int id);

        public void annotated(boolean annotated);

        public Context getContext();

        public void getQuestions(int idPublication);

    }

    interface Interactor {

        interface OnFinishedListener {
            void onFinished();

            void onFailure();
        }

        interface OnFinishedListenerQuestions {
            void onFinishedQuestions(List<QuestionDTO> questions);

            void onFailureQuestions();
        }

        public void getQuestions(int idPublication, OnFinishedListenerQuestions listener);

        public void isAnnotated(int id, OnFinishedListener onFinishedListener);

        public void register(int id, OnFinishedListener onFinishedListener);

        public void rescind(int id, OnFinishedListener onFinishedListener);
    }
}
