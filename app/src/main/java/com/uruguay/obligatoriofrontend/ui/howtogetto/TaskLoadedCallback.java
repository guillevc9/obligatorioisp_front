package com.uruguay.obligatoriofrontend.ui.howtogetto;

public interface TaskLoadedCallback {
    void onTaskDone(Object... values);
}
