package com.uruguay.obligatoriofrontend.ui.showpublicatondetails;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.squareup.picasso.Picasso;
import com.uruguay.obligatoriofrontend.BaseURL;
import com.uruguay.obligatoriofrontend.StaticMenu;
import com.uruguay.obligatoriofrontend.R;
import com.uruguay.obligatoriofrontend.StaticStrings;
import com.uruguay.obligatoriofrontend.adapter.QuestionAdapter;
import com.uruguay.obligatoriofrontend.dto.QuestionDTO;
import com.uruguay.obligatoriofrontend.picasso.PicassoAppConfig;
import com.uruguay.obligatoriofrontend.ui.about.AboutView;
import com.uruguay.obligatoriofrontend.ui.finalizepublication.FinalizePublicationView;
import com.uruguay.obligatoriofrontend.ui.home.HomeView;
import com.uruguay.obligatoriofrontend.ui.howtogetto.MapActivityView;
import com.uruguay.obligatoriofrontend.ui.login.LoginView;
import com.uruguay.obligatoriofrontend.sesion.Session;
import com.uruguay.obligatoriofrontend.ui.newquestion.NewQuestionView;
import com.uruguay.obligatoriofrontend.ui.publication.PublicationView;
import com.uruguay.obligatoriofrontend.ui.showImage.showImageView;
import com.uruguay.obligatoriofrontend.ui.showpublications.ShowPublicationsView;

import java.util.List;

import static com.uruguay.obligatoriofrontend.StaticStrings.FLAG_MYPUBLICATIONS_OR_ANNOTATED;
import static com.uruguay.obligatoriofrontend.StaticStrings.FLAG_MY_PUBLICATIONS;
import static com.uruguay.obligatoriofrontend.StaticStrings.FLAG_MY_PUBLICATIONS_ANNOTATED;
import static com.uruguay.obligatoriofrontend.StaticStrings.ID;
import static com.uruguay.obligatoriofrontend.StaticStrings.ADDRESS;
import static com.uruguay.obligatoriofrontend.StaticStrings.CATEGORY;
import static com.uruguay.obligatoriofrontend.StaticStrings.DESCRIPTION;
import static com.uruguay.obligatoriofrontend.StaticStrings.IMAGE;
import static com.uruguay.obligatoriofrontend.StaticStrings.LOG_TAG;
import static com.uruguay.obligatoriofrontend.StaticStrings.OTHER_REASON;
import static com.uruguay.obligatoriofrontend.StaticStrings.PHOTOS;
import static com.uruguay.obligatoriofrontend.StaticStrings.PUBLISHED_BY_FAMILY_NAME;
import static com.uruguay.obligatoriofrontend.StaticStrings.PUBLISHED_BY_ID;
import static com.uruguay.obligatoriofrontend.StaticStrings.PUBLISHED_BY_NAME;
import static com.uruguay.obligatoriofrontend.StaticStrings.REASON;
import static com.uruguay.obligatoriofrontend.StaticStrings.REASON_NO_USERS;
import static com.uruguay.obligatoriofrontend.StaticStrings.REASON_OTHER;
import static com.uruguay.obligatoriofrontend.StaticStrings.STATE;
import static com.uruguay.obligatoriofrontend.StaticStrings.STATE_ACTIVE;
import static com.uruguay.obligatoriofrontend.StaticStrings.STATE_FINALIZE;
import static com.uruguay.obligatoriofrontend.StaticStrings.TITLE;
import static com.uruguay.obligatoriofrontend.StaticStrings.TITLE_PUBLICATION_DETAILS_VIEW;
import static com.uruguay.obligatoriofrontend.StaticStrings.USER_ASSIGNED;
import static com.uruguay.obligatoriofrontend.StaticStrings.USER_ASSIGNED_ID;
import static com.uruguay.obligatoriofrontend.StaticStrings.ZONE;

public class ShowPublicationDetailsView extends AppCompatActivity implements IShowPublicationDetails.View, Session.OnFinishLogin {

    private static final int PUBLICATION_CODE = 888;
    private static final int QUESTION_CODE = 889;
    private static final int GET_INT_EXTRA_DEFAULT_VALUE = 0;
    private IShowPublicationDetails.Presenter presenter;
    private int idPublication;
    private TextView tvPublishedBy;
    private TextView tvCategory;
    private TextView tvZone;
    private TextView tvTitle;
    private TextView tvDescription;
    private String googleId;
    private Button btnEditSPD;
    private Button btnFinalizeSPD;
    private Button btnNewQuestion;
    private Switch switchSPD;
    private boolean active;
    private int photos;
    private TextView tvState;
    private TextView tvLabelMotiveSPD;
    private TextView tvMotiveSPD;
    private TextView tvLabelUserSPD;
    private TextView tvUserSPD;
    private TextView tvLabelOtherMotiveSPD;
    private TextView tvOtherMotiveSPD;
    private TextView tvLabelAddressSPD;
    private TextView tvAddressSPD;
    private RelativeLayout noDataLayout;
    private RelativeLayout progressBarLayout;
    private ImageView imgView1;
    private ImageView imgView2;
    private ImageView imgView3;
    private Picasso picasso;
    private boolean picture1;
    private boolean picture2;
    private boolean picture3;
    private Button btnHowToGet;
    private String userAssignerId;
    private RecyclerView recyclerViewQuestions;
    private QuestionAdapter adapter;
    private boolean owner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_show_publication_details);
        setTitle(TITLE_PUBLICATION_DETAILS_VIEW);
        initialize();
        setData();
    }

    private void setTitle(String title) {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(title);
        }
    }

    public void clicHowToGet(View v) {
        Intent intent = getIntent();
        Intent intentMap = new Intent(ShowPublicationDetailsView.this, MapActivityView.class);
        intentMap.putExtra(ADDRESS, intent.getStringExtra(ADDRESS));
        startActivity(intentMap);
    }

    private void initialize() {
        Intent intent = getIntent();
        idPublication = intent.getIntExtra(ID, GET_INT_EXTRA_DEFAULT_VALUE);
        presenter = new ShowPublicationDetailsPresenter(this, this);
        tvPublishedBy = findViewById(R.id.tvPublishedBySPD);
        tvCategory = findViewById(R.id.tvCategorySPD);
        tvZone = findViewById(R.id.tvZoneSPD);
        tvTitle = findViewById(R.id.tvTitleSPD);
        tvDescription = findViewById(R.id.tvDescriptionSPD);
        tvState = findViewById(R.id.tvStateSPD);
        btnEditSPD = findViewById(R.id.btnEditSPD);
        btnFinalizeSPD = findViewById(R.id.btnFinalizeSPD);
        btnNewQuestion = findViewById(R.id.btnNewQuestionSPD);
        switchSPD = findViewById(R.id.switchSPD);
        active = intent.getBooleanExtra(STATE, false);
        photos = intent.getIntExtra(PHOTOS, GET_INT_EXTRA_DEFAULT_VALUE);
        tvLabelMotiveSPD = findViewById(R.id.tvLabelMotiveSPD);
        tvMotiveSPD = findViewById(R.id.tvMotiveSPD);
        tvLabelUserSPD = findViewById(R.id.tvLabelUserSPD);
        tvUserSPD = findViewById(R.id.tvUserSPD);
        tvLabelOtherMotiveSPD = findViewById(R.id.tvLabelOtherMotiveSPD);
        tvOtherMotiveSPD = findViewById(R.id.tvOtherMotiveSPD);
        tvLabelAddressSPD = findViewById(R.id.tvLabelAddressSPD);
        tvAddressSPD = findViewById(R.id.tvAddressSPD);
        noDataLayout = findViewById(R.id.noDataLayout);
        progressBarLayout = findViewById(R.id.progressBarLayout);
        imgView1 = findViewById(R.id.imgView1_SPD);
        imgView2 = findViewById(R.id.imgView2_SPD);
        imgView3 = findViewById(R.id.imgView3_SPD);
        picasso = PicassoAppConfig.getPicasso(this);
        picture1 = false;
        picture2 = false;
        picture3 = false;
        imgView1.setVisibility(ImageView.GONE);
        imgView2.setVisibility(ImageView.GONE);
        imgView3.setVisibility(ImageView.GONE);
        btnHowToGet = findViewById(R.id.btnHowToGetSPD);
        userAssignerId = "";
        initializeRecycledView();
    }

    private void initializeRecycledView() {
        recyclerViewQuestions =  findViewById(R.id.recyclerViewQuestions);
        recyclerViewQuestions.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        recyclerViewQuestions.setLayoutManager(llm);
    }

    @Override
    public void setQuestionsToRecyclerView(List<QuestionDTO> questions) {
        adapter = new QuestionAdapter(questions,idPublication,owner,active);
        recyclerViewQuestions.setAdapter(adapter);
    }

    private void update() {
        Log.d(LOG_TAG, "ShowPublicationDetailsView (update): GoogleId ownerpublication: " + googleId);
        Log.d(LOG_TAG, "ShowPublicationDetailsView (update): GoogleId: userloged" + Session.getInstance().getUserGoogleId());
        Log.d(LOG_TAG, "ShowPublicationDetailsView (update): GoogleId userAssignerId: " + userAssignerId);
        hideLoadingScreen();
        if (owner) {
            switchSPD.setVisibility(Switch.GONE);
            btnNewQuestion.setVisibility(Button.GONE);
            if (active) {
                btnEditSPD.setVisibility(Button.VISIBLE);
                btnFinalizeSPD.setVisibility(Button.VISIBLE);
                hideFinalizeFields();
            } else {
                btnEditSPD.setVisibility(Button.GONE);
                btnFinalizeSPD.setVisibility(Button.GONE);
                showFinalizeFields();
            }
        } else {
            btnEditSPD.setVisibility(Button.GONE);
            btnFinalizeSPD.setVisibility(Button.GONE);
            if (active) {
                hideFinalizeFields();
                switchSPD.setVisibility(Switch.VISIBLE);
                btnNewQuestion.setVisibility(Button.VISIBLE);
                initializeSwitch();
            } else {
                switchSPD.setVisibility(Switch.GONE);
                btnNewQuestion.setVisibility(Button.GONE);
                showFinalizeFields();
                if (Session.getInstance().getUserGoogleId().equals(userAssignerId)) {
                    btnHowToGet.setVisibility(Button.VISIBLE);
                    tvLabelAddressSPD.setVisibility(TextView.VISIBLE);
                    tvAddressSPD.setVisibility(TextView.VISIBLE);
                    tvAddressSPD.setText(getIntent().getStringExtra(ADDRESS));
                }
            }
        }
        presenter.getQuestions(idPublication);
    }

    @Override
    protected void onResume() {
        Log.d(LOG_TAG, "ShowPublicationDetailsView: onResume)");
        super.onResume();
        Session.getInstance().silentSign(this, Session.DEFAULT_CODE, this);
    }

    private void showFinalizeFields() {
        String reason = getIntent().getStringExtra(REASON);
        tvLabelMotiveSPD.setVisibility(TextView.VISIBLE);
        tvMotiveSPD.setVisibility(TextView.VISIBLE);
        if (reason.equals(REASON_NO_USERS)) {
            Log.d(LOG_TAG, "ShowPublicationDetailsView: showFinalizeFields: REASON_NO_USERS");
            tvLabelUserSPD.setVisibility(TextView.GONE);
            tvUserSPD.setVisibility(TextView.GONE);
            tvLabelOtherMotiveSPD.setVisibility(TextView.GONE);
            tvOtherMotiveSPD.setVisibility(TextView.GONE);
        } else if (reason.equals(REASON_OTHER)) {
            Log.d(LOG_TAG, "ShowPublicationDetailsView: showFinalizeFields: REASON_OTHER");
            tvLabelUserSPD.setVisibility(TextView.GONE);
            tvUserSPD.setVisibility(TextView.GONE);
            tvLabelOtherMotiveSPD.setVisibility(TextView.VISIBLE);
            tvOtherMotiveSPD.setVisibility(TextView.VISIBLE);

        } else { //REASON_SELECT_CANDIDATE
            Log.d(LOG_TAG, "ShowPublicationDetailsView: showFinalizeFields: REASON_SELECT_CANDIDATE");
            tvLabelUserSPD.setVisibility(TextView.VISIBLE);
            tvUserSPD.setVisibility(TextView.VISIBLE);
            tvLabelOtherMotiveSPD.setVisibility(TextView.GONE);
            tvOtherMotiveSPD.setVisibility(TextView.GONE);
        }
    }

    private void hideFinalizeFields() {
        tvLabelMotiveSPD.setVisibility(TextView.GONE);
        tvMotiveSPD.setVisibility(TextView.GONE);
        tvLabelUserSPD.setVisibility(TextView.GONE);
        tvUserSPD.setVisibility(TextView.GONE);
        tvLabelOtherMotiveSPD.setVisibility(TextView.GONE);
        tvOtherMotiveSPD.setVisibility(TextView.GONE);
    }

    private void initializeSwitch() {
        presenter.isAnnotated(idPublication);
    }

    public void updateSwitch(View view) {
        showLoadingScreen();
        if (switchSPD.isChecked()) {
            presenter.register(idPublication);
        } else {
            presenter.rescind(idPublication);
        }
    }

    public void finalizePublication(View view) {
        Context context = view.getContext();
        Intent intent = getIntent();
        Intent intentFinalize = new Intent(context, FinalizePublicationView.class);
        intentFinalize.putExtra(ID, intent.getIntExtra(ID, GET_INT_EXTRA_DEFAULT_VALUE));
        intentFinalize.putExtra(TITLE, intent.getStringExtra(TITLE));
        intentFinalize.putExtra(DESCRIPTION, intent.getStringExtra(DESCRIPTION));
        intentFinalize.putExtra(ZONE, intent.getStringExtra(ZONE));
        intentFinalize.putExtra(ADDRESS, intent.getStringExtra(ADDRESS));
        intentFinalize.putExtra(CATEGORY, intent.getStringExtra(CATEGORY));
        intentFinalize.putExtra(PUBLISHED_BY_ID, intent.getStringExtra(PUBLISHED_BY_ID));
        intentFinalize.putExtra(PUBLISHED_BY_NAME, intent.getStringExtra(PUBLISHED_BY_NAME));
        intentFinalize.putExtra(PUBLISHED_BY_FAMILY_NAME, intent.getStringExtra(PUBLISHED_BY_FAMILY_NAME));
        startActivityForResult(intentFinalize, PUBLICATION_CODE);
    }

    public void updatePublication(View view) {
        Context context = view.getContext();
        Intent intent = getIntent();
        Intent intentUpdate = new Intent(context, PublicationView.class);
        intentUpdate.putExtra(ID, intent.getIntExtra(ID, GET_INT_EXTRA_DEFAULT_VALUE));
        intentUpdate.putExtra(TITLE, intent.getStringExtra(TITLE));
        intentUpdate.putExtra(DESCRIPTION, intent.getStringExtra(DESCRIPTION));
        intentUpdate.putExtra(ZONE, intent.getStringExtra(ZONE));
        intentUpdate.putExtra(ADDRESS, intent.getStringExtra(ADDRESS));
        intentUpdate.putExtra(CATEGORY, intent.getStringExtra(CATEGORY));
        intentUpdate.putExtra(PUBLISHED_BY_ID, intent.getStringExtra(PUBLISHED_BY_ID));
        intentUpdate.putExtra(PUBLISHED_BY_NAME, intent.getStringExtra(PUBLISHED_BY_NAME));
        intentUpdate.putExtra(PUBLISHED_BY_FAMILY_NAME, intent.getStringExtra(PUBLISHED_BY_FAMILY_NAME));
        intentUpdate.putExtra(PHOTOS, photos);
        intentUpdate.putExtra(StaticStrings.FLAG_UPDATE_PUBLICATION, true);
        startActivityForResult(intentUpdate, PUBLICATION_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PUBLICATION_CODE) {
            finish();
        }
    }

    private void setData() {
        Intent intent = getIntent();
        String name = String.format("%s %s", intent.getStringExtra(PUBLISHED_BY_NAME), intent.getStringExtra(PUBLISHED_BY_FAMILY_NAME));
        googleId = intent.getStringExtra(PUBLISHED_BY_ID);
        tvPublishedBy.setText(name);
        tvTitle.setText(intent.getStringExtra(TITLE));
        tvCategory.setText(intent.getStringExtra(CATEGORY));
        tvZone.setText(intent.getStringExtra(ZONE));
        tvDescription.setText(intent.getStringExtra(DESCRIPTION));
        tvMotiveSPD.setText(intent.getStringExtra(REASON));
        tvUserSPD.setText(intent.getStringExtra(USER_ASSIGNED));
        userAssignerId = intent.getStringExtra(USER_ASSIGNED_ID);
        tvOtherMotiveSPD.setText(intent.getStringExtra(OTHER_REASON));
        String state;
        if (active) {
            state = STATE_ACTIVE;
        } else {
            state = STATE_FINALIZE;
        }
        tvState.setText(state);
        loadImages();
        owner = googleId.equals(Session.getInstance().getUserGoogleId());
        presenter.getQuestions(idPublication);
    }

    private void loadImages() {
        if (photos != 0) {
            for (int i = 1; i <= photos; i++) {
                String url = BaseURL.URL + "publication/" + idPublication + "/downloadPhoto/" + i;
                loadImage(i, url);
            }
        }
    }

    private void loadImage(int i, String url) {
        switch (i) {
            case 1:
                imgView1.setVisibility(ImageView.VISIBLE);
                picasso.load(url).into(imgView1);
                picture1 = true;
                break;
            case 2:
                imgView2.setVisibility(ImageView.VISIBLE);
                picasso.load(url).into(imgView2);
                picture2 = true;
                break;
            case 3:
                imgView3.setVisibility(ImageView.VISIBLE);
                picasso.load(url).into(imgView3);
                picture3 = true;
                break;
        }
    }

    public void clicNewQuestion(View view){
        Intent intent = new Intent(view.getContext(), NewQuestionView.class);
        intent.putExtra(ID, idPublication);
        startActivityForResult(intent, QUESTION_CODE);
    }

    public void showPicture(View view) {
        if (picture1) {
            Intent intent = new Intent(this, showImageView.class);
            String path = BaseURL.URL + "publication/" + idPublication + "/downloadPhoto/1";
            intent.putExtra(StaticStrings.FLAG_LOCAL, false);
            intent.putExtra(IMAGE, path);
            startActivity(intent);
        }
    }

    public void showPicture2(View view) {
        if (picture2) {
            Intent intent = new Intent(this, showImageView.class);
            String path = BaseURL.URL + "publication/" + idPublication + "/downloadPhoto/2";
            intent.putExtra(StaticStrings.FLAG_LOCAL, false);
            intent.putExtra(IMAGE, path);
            startActivity(intent);
        }
    }

    public void showPicture3(View view) {
        if (picture3) {
            Intent intent = new Intent(this, showImageView.class);
            String path = BaseURL.URL + "publication/" + idPublication + "/downloadPhoto/3";
            intent.putExtra(StaticStrings.FLAG_LOCAL, false);
            intent.putExtra(IMAGE, path);
            startActivity(intent);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case R.id.item_home:
                intent = new Intent(this, HomeView.class);
                startActivity(intent);
                break;
            case R.id.item_my_publications:
                intent = new Intent(this, ShowPublicationsView.class);
                intent.putExtra(FLAG_MYPUBLICATIONS_OR_ANNOTATED, FLAG_MY_PUBLICATIONS);
                startActivity(intent);

                break;
            case R.id.item_publications_annotated:
                intent = new Intent(this, ShowPublicationsView.class);
                intent.putExtra(FLAG_MYPUBLICATIONS_OR_ANNOTATED, FLAG_MY_PUBLICATIONS_ANNOTATED);
                startActivity(intent);

                break;
            case R.id.item_about:
                startActivity(new Intent(this, AboutView.class));
                break;
            case R.id.item_logout:
                Session.getInstance().getGoogleSignInClient().signOut()
                        .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                Intent i = new Intent(ShowPublicationDetailsView.this, LoginView.class);
                                startActivity(i);
                            }
                        });
                break;
            case R.id.item_exit:
                StaticMenu.exit(this);
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    @Override
    public void setSwitchOn() {
        switchSPD.setChecked(true);
        hideLoadingScreen();
    }

    @Override
    public void setSwitchOff() {
        hideLoadingScreen();
        switchSPD.setChecked(false);
    }

    @Override
    public void showError(String error) {
        Toast.makeText(ShowPublicationDetailsView.this, error, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showMessage(String messaje) {
        Toast.makeText(ShowPublicationDetailsView.this, messaje, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showLoadingScreen() {
        Log.d(LOG_TAG, "ShowPublicationDetailsView: showLoadingScreen");
        progressBarLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoadingScreen() {
        Log.d(LOG_TAG, "ShowPublicationDetailsView: hideLoadingScreen");
        progressBarLayout.setVisibility(View.GONE);
    }

    @Override
    public void showErrorScreen() {
        Log.d(LOG_TAG, "ShowPublicationDetailsView: showErrorScreen)");
        noDataLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideErrorScreen() {
        Log.d(LOG_TAG, "ShowPublicationDetailsView: hideErrorScreen)");
        noDataLayout.setVisibility(View.GONE);
    }

    @Override
    public void onFinishedLogin(int resultCode) {
        Log.d(LOG_TAG, "ShowPublicationDetailsView: onFinishedLogin)");
        showLoadingScreen();
        hideErrorScreen();
        update();
    }

    @Override
    public void onFailureLogin() {
        Log.d(LOG_TAG, "ShowPublicationDetailsView: onFailureLogin)");
        showErrorScreen();
    }

    public void updateUI(View v) {
        Log.d(LOG_TAG, "ShowPublicationDetailsView: update)");
        hideErrorScreen();
        showLoadingScreen();
        Session.getInstance().silentSign(this, Session.DEFAULT_CODE, this);
    }
}
