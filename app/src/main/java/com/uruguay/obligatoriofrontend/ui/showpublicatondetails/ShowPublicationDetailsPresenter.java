package com.uruguay.obligatoriofrontend.ui.showpublicatondetails;

import android.content.Context;

import com.uruguay.obligatoriofrontend.StaticStrings;
import com.uruguay.obligatoriofrontend.dto.QuestionDTO;

import java.util.List;

import static com.uruguay.obligatoriofrontend.StaticStrings.GET_QUESTIONS_ERROR;


public class ShowPublicationDetailsPresenter implements IShowPublicationDetails.Presenter,
        IShowPublicationDetails.Interactor.OnFinishedListener, IShowPublicationDetails.Interactor.OnFinishedListenerQuestions {

    private IShowPublicationDetails.Interactor interactor;
    private IShowPublicationDetails.View view;
    private Context context;

    public ShowPublicationDetailsPresenter(IShowPublicationDetails.View view, Context context) {
        this.view = view;
        this.interactor = new ShowPublicationDetailsInterceptor(this);
        this.context = context;
    }

    @Override
    public void showError(String error) {
        view.showError(error);
    }

    @Override
    public void isAnnotated(int id) {
        interactor.isAnnotated(id, this);
    }

    @Override
    public void register(int id) {
        interactor.register(id, this);
    }

    @Override
    public void rescind(int id) {
        interactor.rescind(id, this);
    }

    @Override
    public void annotated(boolean annotated) {
        if (annotated) {
            view.setSwitchOn();
        } else {
            view.setSwitchOff();
        }
    }

    @Override
    public Context getContext() {
        return context;
    }

    @Override
    public void getQuestions(int idPublication) {
        interactor.getQuestions(idPublication, this);
    }

    @Override
    public void onFinished() {
        view.showMessage(StaticStrings.OPERATION_OK);
    }

    @Override
    public void onFailure() {
        view.showMessage(StaticStrings.CONECTION_ERROR);
        view.showErrorScreen();
        view.hideLoadingScreen();
    }

    @Override
    public void onFinishedQuestions(List<QuestionDTO> questions) {
        view.setQuestionsToRecyclerView(questions);
    }

    @Override
    public void onFailureQuestions() {
        view.showError(GET_QUESTIONS_ERROR);
    }
}
