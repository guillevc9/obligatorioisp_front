package com.uruguay.obligatoriofrontend.ui.finalizepublication;

import android.content.Context;

import com.uruguay.obligatoriofrontend.dto.FinalizePublicationDTO;
import com.uruguay.obligatoriofrontend.dto.UserDTO;

import java.util.ArrayList;
import java.util.List;

import static com.uruguay.obligatoriofrontend.StaticStrings.FINALIZE_PUBLICATION_OK;
import static com.uruguay.obligatoriofrontend.StaticStrings.GET_CANDIDATES_ERROR;
import static com.uruguay.obligatoriofrontend.StaticStrings.REASON_NO_USERS;
import static com.uruguay.obligatoriofrontend.StaticStrings.REASON_OTHER;
import static com.uruguay.obligatoriofrontend.StaticStrings.REASON_SELECT_CANDIDATE;

public class FinalizePublicationPresenter implements IFinalizePublicationMVP.Presenter , IFinalizePublicationMVP.Interactor.OnFinishedListener , IFinalizePublicationMVP.Interactor.OnFinishedListenerFinalize{

    private IFinalizePublicationMVP.View view;
    private IFinalizePublicationMVP.Interactor interactor;
    private Context context;

    public FinalizePublicationPresenter(IFinalizePublicationMVP.View view, Context context) {
        this.view = view;
        this.context = context;
        this.interactor = new FinalizePublicationIteractor(this);
    }

    @Override
    public void getDataFromServer(int idPublication) {
        interactor.getUsersAnnotated(idPublication,this);
    }

    @Override
    public Context getContext() {
        return context;
    }

    @Override
    public void sendFinalize(FinalizePublicationDTO finalizePublicationDTO) {
        interactor.sendFinalize(finalizePublicationDTO,this);
    }

    @Override
    public void onFinished(List<UserDTO> users) {
        List<String> motives = new ArrayList<>();
        if(users.isEmpty()){
            motives.add(REASON_NO_USERS);

        }else{
            motives.add(REASON_SELECT_CANDIDATE);
            view.showUsersAnnotatedFields();
            view.setUsers(users);
        }
        motives.add(REASON_OTHER);
        view.setMotivesToSpinner(motives);
    }

    @Override
    public void onFailure() {
        view.showError(GET_CANDIDATES_ERROR);
    }

    @Override
    public void onFinishedFinalize() {
        view.showMessage(FINALIZE_PUBLICATION_OK);
        view.finishActiviry();
    }

    @Override
    public void onFailureFinalize(String error) {
        view.showError(error);
    }
}
