package com.uruguay.obligatoriofrontend.ui.login;

import android.content.Context;

public class LoginPresenter implements ILoginMVP.Presenter, ILoginMVP.Interactor.OnFinishedListener {

    private Context context;
    private ILoginMVP.View view;
    private ILoginMVP.Interactor interactor;

    public LoginPresenter(ILoginMVP.View view, Context context) {
        this.view = view;
        this.context = context;
        this.interactor = new LoginInteractor(this);
    }

    @Override
    public Context getContext() {
        return context;
    }

    @Override
    public void sendFirebaseToken() {
        interactor.sendFirebaseToken();
    }

    @Override
    public void authorizeLogin() {
        interactor.authorizeLogin(this);
    }

    @Override
    public void onFinished() {
        view.hideErrorScreen();
        view.hideLoadingScreen();
        view.updateUILogin();
    }

    @Override
    public void onFailure() {
        view.hideLoadingScreen();
        view.showErrorScreen();

    }
}
