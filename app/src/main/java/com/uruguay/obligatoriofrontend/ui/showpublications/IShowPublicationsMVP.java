package com.uruguay.obligatoriofrontend.ui.showpublications;

import android.content.Context;

import com.uruguay.obligatoriofrontend.dto.PublicationDTO;

import java.util.List;

public interface IShowPublicationsMVP {


    interface View {
        public void setPublicationsToRecyclerView(List<PublicationDTO> publications);

        public void showError(String error);

        public void showMessage(String messaje);

        public void getMyPublications();

        public void getMyPublicationsAnnotated();

        void showLoadingScreen();

        void hideLoadingScreen();

        void showErrorScreen();

        void hideErrorScreen();
    }

    interface Presenter {

        public void showError(String error);

        public Context getContext();

        public void getMyPublications();

        public void getMyPublicationsAnnotated();
    }

    interface Interactor {

        interface OnFinishedListener {
            void onFinished(List<PublicationDTO> publications);

            void onFailure();
        }

        public void getMyPublications(IShowPublicationsMVP.Interactor.OnFinishedListener onFinishedListener);

        public void getMyPublicationsAnnotated(IShowPublicationsMVP.Interactor.OnFinishedListener onFinishedListener);
    }

}
