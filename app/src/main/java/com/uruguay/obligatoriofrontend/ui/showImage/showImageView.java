package com.uruguay.obligatoriofrontend.ui.showImage;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.uruguay.obligatoriofrontend.R;
import com.uruguay.obligatoriofrontend.StaticStrings;
import com.uruguay.obligatoriofrontend.picasso.PicassoAppConfig;

import static com.uruguay.obligatoriofrontend.StaticStrings.IMAGE;
import static com.uruguay.obligatoriofrontend.StaticStrings.LOG_TAG;

public class showImageView extends AppCompatActivity {

    private ImageView imageView;
    private boolean local;
    private Picasso picasso;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_show_image);
        imageView = findViewById(R.id.ivShowImage);
        local = getIntent().getBooleanExtra(StaticStrings.FLAG_LOCAL, false);
        picasso = PicassoAppConfig.getPicasso(this);
        loadImage();
    }

    private void loadImage() {
        try {
            String path = getIntent().getStringExtra(IMAGE);
            if (local) {
                Bitmap bitmap = BitmapFactory.decodeFile(path);
                imageView.setImageBitmap(bitmap);
            } else {
                picasso.load(path).into(imageView);
            }
        } catch (Exception e) {
            Log.d(LOG_TAG, e.getMessage());
            finish();
        }
    }

}
