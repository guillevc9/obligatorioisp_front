package com.uruguay.obligatoriofrontend.ui.newquestion;

import android.util.Log;

import com.uruguay.obligatoriofrontend.HttpCodes;
import com.uruguay.obligatoriofrontend.StaticStrings;
import com.uruguay.obligatoriofrontend.dto.QuestionDTO;
import com.uruguay.obligatoriofrontend.retrofit.IRetrofitApiConfigMVP;
import com.uruguay.obligatoriofrontend.retrofit.RetrofitAppConfig;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.uruguay.obligatoriofrontend.StaticStrings.LOG_TAG;

public class NewQuestionInteractor implements INewQuestionMVP.Interactor {

    private INewQuestionMVP.Presenter presenter;

    public NewQuestionInteractor(INewQuestionMVP.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void newQuestion(int idPublication, QuestionDTO question, final OnFinishedListener onFinishedListener) {
        IRetrofitApiConfigMVP getResponse = RetrofitAppConfig.getRetrofit(presenter.getContext()).create(IRetrofitApiConfigMVP.class);
        String url = String.format("publication/%s/question", idPublication);
        Log.d(LOG_TAG, "NewQuestionInteractor: newQuestion ID: " + idPublication + " URL: " + url);
        Log.d(LOG_TAG, "NewQuestionInteractor: newQuestion question: " + question);
        Call<Void> call = getResponse.newQuestion(question, url);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Log.d(LOG_TAG, "NewQuestionInteractor: newQuestion response.code(): " + response.code());
                if (response.code() == HttpCodes.OK) {

                    Log.d(LOG_TAG, "NewQuestionInteractor: onResponse OK");
                    onFinishedListener.onFinished();
                }  else if(response.code() == HttpCodes.UNAUTHORIZED){
                    presenter.showError(StaticStrings.UNAUTHORIZED);
                }else {
                    Log.d(LOG_TAG, "NewQuestionInteractor: onResponse ELSE");
                    onFinishedListener.onFailure();
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.d(LOG_TAG, "NewQuestionInteractor: onFailure");
                onFinishedListener.onFailure();
            }
        });
    }

}
