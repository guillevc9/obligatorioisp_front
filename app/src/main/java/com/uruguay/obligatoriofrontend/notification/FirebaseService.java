package com.uruguay.obligatoriofrontend.notification;

import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.uruguay.obligatoriofrontend.sesion.Session;

import static com.uruguay.obligatoriofrontend.StaticStrings.LOG_TAG;

public class FirebaseService extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.d(LOG_TAG, "onMessageReceived");
        Log.d(LOG_TAG, "From: " + remoteMessage.getFrom());
        if (remoteMessage.getNotification() != null) {
            String title = remoteMessage.getNotification().getTitle();
            String body = remoteMessage.getNotification().getBody();
            Log.d(LOG_TAG, "Message Notification Body: " + body);
            Notification.newNotification(this, Session.getInstance().getNextIdMessage(), title, body);
        }
    }

    @Override
    public void onNewToken(String token) {
        Log.d(LOG_TAG, "Refreshed token: " + token);
        SendTokenToServer.sendTokenToServer(this, token);
    }


}
