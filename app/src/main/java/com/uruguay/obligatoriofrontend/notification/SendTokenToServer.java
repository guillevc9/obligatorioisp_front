package com.uruguay.obligatoriofrontend.notification;

import android.content.Context;
import android.util.Log;

import com.uruguay.obligatoriofrontend.HttpCodes;
import com.uruguay.obligatoriofrontend.retrofit.IRetrofitApiConfigMVP;
import com.uruguay.obligatoriofrontend.retrofit.RetrofitAppConfig;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.uruguay.obligatoriofrontend.StaticStrings.LOG_TAG;

public final class SendTokenToServer {

    public static void sendTokenToServer(Context context, String token) {
        Log.d(LOG_TAG, "sendTokenToServer Token: " + token);
        IRetrofitApiConfigMVP getResponse = RetrofitAppConfig.getRetrofit(context).create(IRetrofitApiConfigMVP.class);
        Call<Void> call = getResponse.sendFirebaseToken(token);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.code() == HttpCodes.OK) {
                    Log.d(LOG_TAG, "sendTokenToServer: onResponse code OK");
                } else {
                    Log.d(LOG_TAG, "sendTokenToServer: onResponse ELSE");
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.d(LOG_TAG, "sendTokenToServer: onFailure");
            }
        });
    }


}
