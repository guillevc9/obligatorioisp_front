package com.uruguay.obligatoriofrontend.notification;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;

import com.uruguay.obligatoriofrontend.R;

import static com.uruguay.obligatoriofrontend.StaticStrings.LOG_TAG;

public class Notification {

    private static final String CHANNEL_ID = "1234";
    private static final String CHANNEL_NAME = "canalobligatorio";
    private static final String CHANNEL_DESCRIPTION = "Canal de envio de mensajes del obligatorio";
    private static final int ID_MESSAGE = 3;

    public static void newNotification(Context context, int idMessage, String title, String body) {
        createNotificationChannel(context);
        Log.d(LOG_TAG, "Message Notification Body: " + body);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, CHANNEL_ID)
                .setSmallIcon(R.drawable.notification)
                .setContentTitle(title)
                .setContentText(body)
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(body))
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
        notificationManager.notify(ID_MESSAGE, builder.build());
    }

    private static void createNotificationChannel(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, CHANNEL_NAME, importance);
            channel.setDescription(CHANNEL_DESCRIPTION);
            NotificationManager notificationManager = context.getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

}
