package com.uruguay.obligatoriofrontend.sesion;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.uruguay.obligatoriofrontend.BuildConfig;
import com.uruguay.obligatoriofrontend.dto.CategoryDTO;
import com.uruguay.obligatoriofrontend.dto.ZoneDTO;

import java.util.ArrayList;
import java.util.List;

import static com.uruguay.obligatoriofrontend.StaticStrings.LOG_TAG;


public class Session {

    public static final int DEFAULT_CODE = 42;
    private static final Session ourInstance = new Session();
    private GoogleSignInClient googleSignInClient;
    private GoogleSignInAccount googleAccount;
    private List<CategoryDTO> categories;
    private List<ZoneDTO> zones;
    private String token;
    private int nextIdMessage;

    public static Session getInstance() {
        return ourInstance;
    }

    private String userGoogleId;
    private String name;
    private String familyName;
    private String mail;

    public interface OnFinishLogin {
        void onFinishedLogin(int resultCode);
        void onFailureLogin();
    }

    private Session() {
        this.categories = new ArrayList<>();
        this.zones = new ArrayList<>();
        this.userGoogleId = "";
        this.name = "";
        this.familyName = "";
        this.mail = "";
        this.googleAccount = null;
        this.googleSignInClient = null;
        this.token = "";
        this.nextIdMessage = 1;
    }

    public int getNextIdMessage() {
        return nextIdMessage++;
    }

    public static Session getOurInstance() {
        return ourInstance;
    }

    public void initializeGoogleClient(Activity activity) {
        String api_key = BuildConfig.ApiKey;
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(api_key)
                .requestEmail()
                .build();
        this.setGoogleSignInClient(GoogleSignIn.getClient(activity, gso));
        Log.d(LOG_TAG, "Session initializeGoogleClient");
    }

    public String getToken() {
        return this.token;
    }

    public void silentSign(final OnFinishLogin onFinishLogin, final int code, Activity activity) {
        Task<GoogleSignInAccount> task = Session.getInstance().getGoogleSignInClient().silentSignIn();
        if (task.isSuccessful()) {
            Session.getInstance().setGoogleAccount(task.getResult());
            Session.getInstance().setToken(task.getResult().getIdToken());
            Log.w(LOG_TAG, "Session:silentSign:token: " + task.getResult().getIdToken());
            setUserData();
            onFinishLogin.onFinishedLogin(code);

        } else {
            task.addOnCompleteListener(activity, new OnCompleteListener<GoogleSignInAccount>() {
                @Override
                public void onComplete(@NonNull Task<GoogleSignInAccount> task) {
                    try {
                        Log.d(LOG_TAG, "Session: En onComplete en silentSign");
                        Session.getInstance().setGoogleAccount(task.getResult(ApiException.class));
                        Session.getInstance().setToken(task.getResult().getIdToken());
                        Log.w(LOG_TAG, "Session:silentSign:token: " + task.getResult().getIdToken());
                        setUserData();
                        onFinishLogin.onFinishedLogin(code);

                    } catch (ApiException e) {
                        Log.w(LOG_TAG, "Session:error", e);
                        Log.w(LOG_TAG, "Session:error" + e.getMessage());
                        onFinishLogin.onFailureLogin();
                    }
                }
            });
        }
    }

    public void signIn(final OnFinishLogin onFinishLogin, Intent data){
        Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
        try {
            GoogleSignInAccount account = task.getResult(ApiException.class);
            Session.getInstance().setGoogleAccount(account);
            if (account != null) {
                setUserData();
                onFinishLogin.onFinishedLogin(DEFAULT_CODE);
            }
        } catch (ApiException e) {
            Log.d(LOG_TAG, "Session: signIn  Result:failed code=" + e.getStatusCode());
            Session.getInstance().setGoogleAccount(null);
            onFinishLogin.onFailureLogin();
        }
    }

    private void setUserData() {
        Log.d(LOG_TAG, "Session setUserData:" + this.googleAccount.getId());
       this.setName(this.googleAccount.getGivenName());
        this.setFamilyName(this.googleAccount.getFamilyName());
        this.setMail(this.googleAccount.getEmail());
        this.setUserGoogleId(this.googleAccount.getId());
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUserGoogleId() {
        return userGoogleId;
    }

    public void setUserGoogleId(String userGoogleId) {
        this.userGoogleId = userGoogleId;
    }

    public GoogleSignInClient getGoogleSignInClient() {
        return googleSignInClient;
    }

    public void setGoogleSignInClient(GoogleSignInClient googleSignInClient) {
        this.googleSignInClient = googleSignInClient;
    }

    public GoogleSignInAccount getGoogleAccount() {
        return googleAccount;
    }

    public void setGoogleAccount(GoogleSignInAccount googleAccount) {
        this.googleAccount = googleAccount;
    }

    public List<CategoryDTO> getCategories() {
        return categories;
    }

    public void setCategories(List<CategoryDTO> categories) {
        this.categories = categories;
    }

    public List<ZoneDTO> getZones() {
        return zones;
    }

    public void setZones(List<ZoneDTO> zones) {
        this.zones = zones;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

}
