package com.uruguay.obligatoriofrontend;

public class OperationResult {

    public boolean isValid;
    public String message;

    public OperationResult(){
        isValid = true;
        message = "";
    }

}
